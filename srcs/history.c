/*
** history.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May 13 10:52:33 2015 lionel karmes
** Last update Sun May 24 15:15:48 2015 VIctor Le
*/

#include <string.h>
#include "history.h"

int		history(t_list_hist *list_hist, char *cmd)
{
  epur_str(cmd);
  delete_backspace(cmd);
  if (cmd[0] == '\0')
    {
      free(cmd);
      return (1);
    }
  if (list_hist->tail != NULL)
    {
      if (!strcmp(list_hist->tail->cmd, cmd))
	{
	  free(cmd);
	  return (1);
	}
    }
  if (!t_list_hist_append_hist(list_hist, cmd))
    return (0);
  return (1);
}

char		*find_hist(const t_list_hist *list_hist, int index)
{
  t_hist	*tmp;
  int		i;

  if (list_hist == NULL)
    return (NULL);
  tmp = NULL;
  if (index < 0)
    index = list_hist->id_current + index + 1;
  if (index > list_hist->id_current)
    index = list_hist->id_current;
  if (index <= list_hist->id_current - list_hist->list_size)
    return (NULL);
  i = list_hist->id_current - list_hist->list_size;
  tmp = list_hist->head;
  while (i++ < index - 1)
    tmp = tmp->next;
  return (tmp->cmd);
}
