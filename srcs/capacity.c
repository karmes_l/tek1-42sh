/*
** capacity.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May 15 10:39:28 2015 lionel karmes
** Last update Fri Jun  5 18:10:52 2015 VIctor Le
*/

#include <stdlib.h>
#include "termcaps.h"

void		aff_str(int fd, t_str *str)
{
  t_char	*tmp;

  tmp = str->head;
  while (tmp != str->tail)
    {
      write(fd, &tmp->c, 1);
      tmp = tmp->next;
    }
}

void		pos_cursor_current(int fd, t_str *str, t_char *current)
{
  char		*cap;
  t_char	*tmp;

  if (current)
    {
      tmp = str->current;
      while (tmp != str->head)
	{
	  cap = tgetstr("le", NULL);
	  dprintf(fd, "%s", cap);
	  tmp = tmp->prev;
	}
      while (tmp != current && tmp != str->tail)
	{
	  cap = tgetstr("nd", NULL);
	  dprintf(fd, "%s", cap);
	  tmp = tmp->next;
	}
    }
}

void		update_line(int fd, t_str *str)
{
  char		*cap;
  t_char	*tmp;

  cap = tgetstr("ce", NULL);
  dprintf(fd, "%s", cap);
  aff_str(fd, str);
  tmp = str->tail;
  while (tmp != str->current)
    {
      cap = tgetstr("le", NULL);
      dprintf(fd, "%s", cap);
      tmp = tmp->prev;
    }
}

void		clear_tty(int fd, t_str *str, char *prompt)
{
 char		*cap;

  cap = tgetstr("cl", NULL);
  dprintf(fd, "%s", cap);
  dprintf(fd, prompt);
  aff_str(fd, str);
  pos_cursor_current(fd, str, str->current);
}
