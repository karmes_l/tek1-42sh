/*
** t_sh.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May  4 21:45:38 2015 VIctor Le
** Last update Thu May 21 14:31:31 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_sh.h"
#include "env.h"
#include "str_to_wdtab.h"
#include "free_and_null.h"
#include "t_list_hist.h"

void	t_sh_constructor(t_sh *sh)
{
  t_env_constructor(&sh->env);
  t_list_hist_constructor(&sh->hist);
  sh->cmd_return = 0;
  sh->ask_exit = 0;
  sh->prompt = "$>";
}

static void	t_env_destructor(t_env *env)
{
  if (env != NULL)
    {
      free_wdtab(env->env);
      free_wdtab(env->path);
      free_and_null((void **)&env->home);
      free_and_null((void **)&env->pwd);
      free_and_null((void **)&env->oldpwd);
    }
}

void	t_sh_destructor(t_sh *sh)
{
  t_env_destructor(&sh->env);
  t_list_hist_destructor(&sh->hist);
}
