/*
** rec_env.c for rec_env in /home/panya_s/Unix_system_Programminig/env
**
** Made by Saysana Panya
** Login   <panya_s@epitech.net>
**
** Started on  Mon May 11 16:19:55 2015 Saysana Panya
** Last update Sun Jun  7 22:00:10 2015 VIctor Le
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "env.h"

void	t_env_constructor(t_env *my_env)
{
  my_env->path = NULL;
  my_env->home = NULL;
  my_env->pwd = NULL;
  my_env->oldpwd = NULL;
  my_env->env = NULL;
}

int	recup_env(char **env, t_env *my_env)
{
  int	i;

  i = 0;
  while (env[i] != NULL)
    ++i;
  if ((my_env->env = malloc(sizeof(char *) * (i + 1))) == NULL)
    return (-2);
  my_env->env[i] = NULL;
  i = 0;
  while (env[i] != NULL)
    {
      if ((my_env->env[i] = strdup(env[i])) == NULL)
	return (-2);
      if (strncmp(env[i], "HOME", 4) == 0
	  && (my_env->home = strdup(env[i] + 5)) == NULL)
	return (-2);
      else if (strncmp(env[i], "PWD", 3) == 0
	       && (my_env->pwd = strdup(env[i] + 4)) == NULL)
	return (-2);
      else if (strncmp(env[i], "OLDPWD", 6) == 0
	       && (my_env->oldpwd = strdup(env[i] + 7)) == NULL)
	return (-2);
      ++i;
    }
  return (0);
}

int	cp_path(t_env *my_env, int i)
{
  int	j;
  int	k;
  char	*tmp;

  k = 0;
  j = 0;
  tmp = my_env->env[i] + 5;
  while (tmp[j] != '\0')
    {
      while (tmp[j] != '\0' && tmp[j] != ':')
	j++;
      if ((my_env->path[k] = strndup(tmp, j)) == NULL)
	return (-2);
      tmp = (tmp[j] != '\0') ? tmp + j + 1 : tmp + j;
      ++k;
      j = 0;
    }
  return (0);
}

int	set_path(t_env *my_env)
{
  int	i;
  int	j;
  int	k;

  i = 0;
  j = 0;
  k = 1;
  while (my_env->env[i] && (strncmp(my_env->env[i], "PATH", 4)) != 0)
    i++;
  if (!my_env->env[i])
    return (1);
  while (my_env->env[i][j] != '\0')
    {
      if (my_env->env[i][j] == ':')
	++k;
      ++j;
    }
  if ((my_env->path = malloc(sizeof(char *) * (k + 1))) == NULL)
    return (-2);
  my_env->path[k] = NULL;
  if ((cp_path(my_env, i)) == -2)
    return (-2);
  return (0);
}

int	to_env(char **env, t_env *my_env)
{
  t_env_constructor(my_env);
  if (recup_env(env, my_env) == -2)
    return (-2);
  if ((set_path(my_env)) == -2)
    return (-2);
  return (0);
}
