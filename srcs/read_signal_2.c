/*
** read_signal_2.c for 42sh in /home/le_l/workspace/42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 29 16:34:23 2015 VIctor Le
** Last update Fri May 29 16:34:59 2015 VIctor Le
*/

#include <sys/wait.h>
#include <unistd.h>

void	get_sigint_dad(int sign)
{
  (void)sign;
  (void)write(1, "\n$>", 3);
}

void	get_sigint_son(int sign)
{
  (void)sign;
}

void	get_sigquit_dad(int sign)
{
  (void)sign;
  (void)write(1, "\n$>", 3);
}

void	get_sigquit_son(int sign)
{
  (void)sign;
  (void)write(1, "Quit\n", 5);
}

void	get_sigchld(int sign)
{
  pid_t	pid;

  (void)sign;
  pid = 1;
  while (pid > 0)
    pid = waitpid(-1, NULL, WNOHANG);
}
