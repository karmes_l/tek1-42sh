/*
** exec_builtin.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:41:15 2015 VIctor Le
** Last update Sun May 24 15:54:20 2015 VIctor Le
*/

#include <stdlib.h>
#include "exec_cmd.h"
#include "t_cmd.h"
#include "t_sh.h"
#include "redirection.h"
#include "builtins.h"

static void	fill_t_builtin_fptr(t_builtin *fptr)
{
  fptr[0] = &builtin_cd;
  fptr[1] = &builtin_echo;
  fptr[2] = &builtin_env;
  fptr[3] = &builtin_setenv;
  fptr[4] = &builtin_unsetenv;
  fptr[5] = &builtin_exit;
  fptr[6] = &builtin_history;
  fptr[7] = NULL;
}

static int	dup_stdin_stdout(int *stdin, int *stdout, int ask_dup)
{
  if (ask_dup)
    {
      if ((*stdin = dup(0)) == -1 || (*stdout = dup(0)) == -1)
	return (0);
    }
  else
    {
      if (dup2(*stdin, 0) == -1 || dup2(*stdout, 1) == -1)
	return (0);
    }
  return (1);
}

int		exec_builtin(t_cmd *cmd, t_sh *sh, int i)
{
  t_builtin	fptr[NB_BUILTIN + 1];
  int		stdin;
  int		stdout;

  fill_t_builtin_fptr(fptr);
  if (!dup_stdin_stdout(&stdin, &stdout, 1))
    return (0);
  if ((here_doc(cmd)) == FAIL || handle_redirections(cmd) == FAIL)
    {
      dup_stdin_stdout(&stdout, &stdin, 0);
      return (EXEC_ERROR);
    }
  if (i > -1 && i < NB_BUILTIN)
    cmd->ret = fptr[i](cmd, sh);
  sh->cmd_return = cmd->ret;
  dup_stdin_stdout(&stdout, &stdin, 0);
  return (EXEC_OK);
}
