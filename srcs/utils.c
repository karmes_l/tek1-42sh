/*
** char_printable.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May 15 00:02:14 2015 lionel karmes
** Last update Mon May 18 13:14:29 2015 VIctor Le
*/

#include "key_events.h"

int	char_printable(char c)
{
  if ((c > 31 && c < 127) || c == '\n')
    return (1);
  return (0);
}

int	is_suppr(char c[4])
{
  if (c[0] == 27 && c[1] == 91 && c[2] == 51 && c[3] == 126)
    return (1);
  return (0);
}

int	is_arrow(char c[4])
{
  if (c[0] == 27 && c[1] == 91 && c[3] == 0)
    {
      if (c[2] == 65)
	return (K_UP);
      else if (c[2] == 66)
	return (K_DOWN);
      else if (c[2] == 67)
	return (K_RIGHT);
      else if (c[2] == 68)
	return (K_LEFT);
    }
  return (0);
}

int	is_begin(char c[4])
{
  if (c[0] == 27 && c[1] == 79 && c[2] == 72 && c[3] == 0)
    return (1);
  return (0);
}

int	is_end(char c[4])
{
  if (c[0] == 27 && c[1] == 79 && c[2] == 70 && c[3] == 0)
    return (1);
  return (0);
}
