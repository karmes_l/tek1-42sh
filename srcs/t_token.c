/*
** t_token.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:26:49 2015 VIctor Le
** Last update Sat May  9 16:17:06 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_token.h"
#include "free_and_null.h"

void	t_token_constructor(t_token *token)
{
  token->next = NULL;
  token->prev = NULL;
  token->token = NULL;
  token->isword = 1;
}

t_token		*t_token_destructor(t_token *token)
{
  t_token	*ret;

  ret = NULL;
  if (token != NULL)
    {
      ret = token->next;
      free_and_null((void **)&token->token);
      if (token->next != NULL)
	token->next->prev = token->prev;
      if (token->prev != NULL)
	token->prev->next = token->next;
      free_and_null((void **)&token);
    }
  return (ret);
}
