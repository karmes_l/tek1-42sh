/*
** metachar_handler3.c for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May  8 19:56:25 2015 VIctor Le
** Last update Mon May 11 12:34:00 2015 VIctor Le
*/

#include "metachar_handler.h"
#include "t_str.h"
#include "t_list.h"
#include "t_op.h"

int	blank_handler(char *s, int *i, t_str *str, t_list *list)
{
  (void)str;
  (void)list;
  while (s[*i] == ' ' || s[*i] == '\t')
    ++(*i);
  return (META_OK);
}

int	newline_handler(char *s, int *i, t_str *str, t_list *list)
{
  if (!t_str_append_char(str, s[*i]))
    return (META_FAIL);
  ++(*i);
  if (!push_back_token(str, list->tail, 0) || !operator_handler(list))
    return (META_FAIL);
  return (META_OK);
}
