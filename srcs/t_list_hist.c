/*
** t_list_hist.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May 13 10:36:16 2015 lionel karmes
** Last update Thu May 21 14:35:24 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_list_hist.h"
#include "t_hist.h"
#include "free_and_null.h"

void	t_list_hist_constructor(t_list_hist *list_hist)
{
  list_hist->head = NULL;
  list_hist->tail = NULL;
  list_hist->it = NULL;
  list_hist->id_current = 0;
  list_hist->list_size = 0;
}

int		t_list_hist_append_hist(t_list_hist *list_hist, char *cmd)
{
  t_hist	*node;

  if (!list_hist || !cmd || !(node = malloc(sizeof(*node))))
    return (0);
  t_hist_constructor(node);
  node->cmd = cmd;
  node->prev = list_hist->tail;
  if (list_hist->head == NULL)
    list_hist->head = node;
  if (list_hist->tail != NULL)
    list_hist->tail->next = node;
  list_hist->tail = node;
  ++list_hist->list_size;
  ++list_hist->id_current;
  return (1);
}

void	t_list_hist_remove_hist(t_list_hist *list_hist, t_hist *hist)
{
  if (list_hist != NULL && hist != NULL)
    {
      if (hist == list_hist->head)
	list_hist->head = t_hist_destructor(hist);
      else if (hist == list_hist->tail)
	{
	  list_hist->tail = list_hist->tail->prev;
	  (void)t_hist_destructor(hist);
	}
      else
	(void)t_hist_destructor(hist);
      --list_hist->list_size;
      if (list_hist->list_size == 0)
	list_hist->tail = NULL;
    }
}

void	t_list_hist_destructor(t_list_hist *list_hist)
{
  if (list_hist != NULL)
    {
      while (list_hist->head != NULL && list_hist->list_size > 0)
	t_list_hist_remove_hist(list_hist, list_hist->head);
    }
}
