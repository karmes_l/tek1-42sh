/*
** lexer.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh/workspace/parser
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Mon May 11 12:28:29 2015 Aurélien Metz
** Last update Sun May 24 15:26:06 2015 VIctor Le
*/

#include "lexer.h"

int		find_word(const t_token *token)
{
  const t_token	*save;

  save = token;
  if (token == NULL)
    return (-1);
  while (token)
    {
      if (token->isword)
	return (token == save ? 0 : 1);
      token = token->next;
    }
  return (-1);
}

static int	check_ops(const t_cmd *cmd)
{
  if ((cmd->operator[1] == OP_PIPE
       || cmd->operator[1] == OP_OR
       || cmd->operator[1] == OP_AND)
      && (cmd->next == NULL
	  || (find_word(cmd->head) == -1
	      || find_word(cmd->next->head) == -1)))
    return (fprintf(stderr, CMD_NULL));
  return (0);
}

static int	check_dir(const t_token *token, const char red)
{
  unsigned int	i;

  while (token)
    {
      i = 0;
      while (token->token[i])
	if (token->token[i++] == red)
	  {
	    fprintf(stderr, CMD_AMB, red == LEFT_RED ? INPUT : OUTPUT);
	    return (0);
	  }
      token = token->next;
    }
  return (1);
}

static int	check_red(const t_cmd *cmd)
{
  t_token	*token;
  char		output;
  char		input;

  output = 0;
  input = 0;
  if (cmd->operator[1] == OP_PIPE && check_dir(cmd->head, '>') == 0)
    return (-1);
  token = cmd->head;
  while (token)
    {
      if (token->isword == 0)
	if (check_token(token, &output, &input))
	  return (-1);
      token = token->next;
    }
  if (cmd->operator[1] == OP_PIPE && check_dir(cmd->next->head, '<') == 0)
    return (-1);
  return (0);
}

int		lexer(const t_list *list)
{
  t_cmd		*cmd;

  cmd = list->head;
  while (cmd)
    {
      if (check_ops(cmd) || check_red(cmd))
	return (0);
      cmd = cmd->next;
    }
  return (1);
}
