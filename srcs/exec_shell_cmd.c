/*
** exec_shell_cmd.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 08:21:13 2015 VIctor Le
** Last update Sun Jun  7 15:34:32 2015 VIctor Le
*/

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "exec_cmd.h"
#include "free_and_null.h"
#include "redirection.h"
#include "read_signal.h"

/*
** Execution d'une commande shell.
** Retourne dans TOUS les cas EXEC_PIPE.
*/
static int	in_child_execution(char *bin_path, t_cmd *cmd, char **env)
{
  char		**argv;

  if (here_doc(cmd) == FAIL || handle_redirections(cmd) == FAIL
      || (argv = t_cmd_get_argv(cmd)) == NULL)
    {
      free_and_null((void **)&bin_path);
      return (EXEC_PIPE);
    }
  if (setsid() == -1)
    fprintf(stderr, "%s\n", strerror(errno));
  (void)execve(bin_path, argv, env);
  free_and_null((void **)&bin_path);
  t_cmd_free_argv(argv);
  return (EXEC_PIPE);
}

int	exec_shell_cmd(t_cmd *cmd, t_sh *sh, char *bin_path, int forked)
{
  pid_t	pid;

  if (forked)
    return (in_child_execution(bin_path, cmd, sh->env.env));
  read_signal(PROC_SON);
  if ((pid = fork()) == -1)
    {
      fprintf(stderr, "%s\n", strerror(errno));
      return (EXEC_ERROR);
    }
  if (!pid)
    return (in_child_execution(bin_path, cmd, sh->env.env));
  if (wait(&cmd->ret) == -1)
    fprintf(stderr, "wait error\n");
  if (WIFSIGNALED(cmd->ret) && WTERMSIG(cmd->ret) == 11)
    printf("Segmentation fault\n");
  free_and_null((void **)&bin_path);
  sh->cmd_return = cmd->ret;
  read_signal(PROC_DAD);
  return (EXEC_OK);
}
