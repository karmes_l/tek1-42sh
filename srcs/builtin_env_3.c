/*
** builtin_env_3.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Fri May 22 11:29:16 2015 Aurélien Metz
** Last update Sun May 24 15:18:48 2015 VIctor Le
*/

#include <stdio.h>
#include "str_to_wdtab.h"

int	env_err(char **env, const char *err)
{
  fprintf(stderr, "env: %s\n", err);
  free_wdtab(env);
  return (1);
}
