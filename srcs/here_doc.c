/*
** here_doc.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May  8 18:10:31 2015 lionel karmes
** Last update Sat Jun  6 11:03:13 2015 VIctor Le
*/

#include <stdio.h>
#include <stdlib.h>
#include "gnl.h"
#include "redirection.h"
#include "free_and_null.h"

static int	double_left_loop(t_cmd *cmd, int i)
{
  if ((cmd->buffer =
       realloc(cmd->buffer, sizeof(*cmd->buffer) * (i + 1))) == NULL
      || (cmd->buffer[i] = gnl(0)) == NULL)
    return (FAIL);
  if (cmd->buffer[i][0] == '\0')
    return (END_HERE_DOC);
  return (OK);
}

static int	here_doc_error(int fd_tty, int open_case, char **line)
{
  if (open_case)
    fprintf(stderr, strerror(errno));
  free_and_null((void **)line);
  write(fd_tty, "\n", 1);
  close_fd(fd_tty);
  return (FAIL);
}

static int	double_left_case(t_cmd *cmd, t_token *token)
{
  int		i;
  int		fd_tty;
  int		ret;

  fd_tty = -1;
  if ((fd_tty = open("/dev/tty", O_WRONLY)) == -1)
    return (here_doc_error(fd_tty, 1, NULL));
  i = 0;
  write(fd_tty, "? ", 2);
  if ((ret = double_left_loop(cmd, i)) == FAIL)
    return (here_doc_error(fd_tty, 0, &cmd->buffer[i]));
  while (ret == OK
	 && strncmp(cmd->buffer[i], token->token, strlen(token->token)))
    {
      ++i;
      write(fd_tty, "? ", 2);
      if ((ret = double_left_loop(cmd, i)) == FAIL)
	return (here_doc_error(fd_tty, 0, &cmd->buffer[i]));
      else if (ret == END_HERE_DOC)
	break ;
    }
  free_and_null((void **)&cmd->buffer[i]);
  close_fd(fd_tty);
  return (OK);
}

int		here_doc(t_cmd *cmd)
{
  t_token	*token;

  token = cmd->head;
  while (token)
    {
      if (!(token->isword) && !(strcmp(CHEV_DL, token->token)))
	{
	  free_and_null((void **)&cmd->buffer);
	  if (double_left_case(cmd, token->next) == FAIL)
	    return (FAIL);
	}
      if (!(token->isword) && !(strcmp(CHEV_TL, token->token)))
	{
	  free_and_null((void **)&cmd->buffer);
	  if ((*cmd->buffer = strdup(token->next->token)) == NULL)
	    return (FAIL);
	}
      token = token->next;
    }
  return (OK);
}
