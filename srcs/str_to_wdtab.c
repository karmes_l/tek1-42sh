/*
** str_to_wdtab.c for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 08:41:31 2015 VIctor Le
** Last update Tue May 19 23:21:05 2015 VIctor Le
*/

#include <string.h>
#include <stdlib.h>
#include "free_and_null.h"

static char	**add_word(char **wdtab, int *size, char *s, int len)
{
  ++(*size);
  if ((wdtab = realloc(wdtab, sizeof(*wdtab) * *size)) == NULL
      || (wdtab[*size - 1] = strndup(s, len)) == NULL)
    return (NULL);
  return (wdtab);
}

static char	**add_null(char **wdtab, int size)
{
  ++size;
  if ((wdtab = realloc(wdtab, sizeof(*wdtab) * size)) == NULL)
    return (NULL);
  wdtab[size - 1] = NULL;
  return (wdtab);
}

static int	is_delim(char c, char *delim)
{
  int		i;

  i = 0;
  while (delim[i] != '\0' && c != delim[i])
    ++i;
  if (delim[i] == '\0')
    return (0);
  return (1);
}

char	**str_to_wdtab(char *s, char *delim)
{
  char	**wdtab;
  int	size;
  int	i;
  int	len;

  if (s == NULL || delim == '\0')
    return (NULL);
  wdtab = NULL;
  size = 0;
  i = 0;
  while (s[i] != '\0')
    {
      len = 0;
      while (s[i + len] != '\0' && !is_delim(s[i + len], delim))
	++len;
      if ((wdtab = add_word(wdtab, &size, s + i, len)) == NULL)
	return (NULL);
      len = (is_delim(s[i + len], delim)) ? len + 1 : len;
      i += len;
    }
  if ((wdtab = add_null(wdtab, size)) == NULL)
    return (NULL);
  return (wdtab);
}

void	free_wdtab(char **wdtab)
{
  int	i;

  if (wdtab != NULL)
    {
      i = 0;
      while (wdtab[i] != NULL)
	{
	  free_and_null((void **)&wdtab[i]);
	  ++i;
	}
      free_and_null((void **)&wdtab);
    }
}
