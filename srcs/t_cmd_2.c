/*
** t_cmd_2.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 22:47:04 2015 VIctor Le
** Last update Sun May 24 15:24:43 2015 VIctor Le
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "free_and_null.h"
#include "t_cmd.h"

char		**t_cmd_get_argv(t_cmd *cmd)
{
  char		**argv;
  t_token	*tmp;
  int		i;

  if (cmd->list_size <= 0)
    return (NULL);
  if ((argv = malloc(sizeof(*argv) * (cmd->list_size + 1))) == NULL)
    return (NULL);
  i = 0;
  tmp = cmd->head;
  while (i < cmd->list_size)
    {
      if ((argv[i] = strdup(tmp->token)) == NULL)
	{
	  fprintf(stderr, "%s\n", strerror(errno));
	  return (NULL);
	}
      tmp = tmp->next;
      ++i;
    }
  argv[i] = NULL;
  return (argv);
}

void	t_cmd_free_argv(char **argv)
{
  int	i;

  if (argv != NULL)
    {
      i = 0;
      while (argv[i] != NULL)
	{
	  free_and_null((void **)&argv[i]);
	  ++i;
	}
      free_and_null((void **)&argv);
    }
}
