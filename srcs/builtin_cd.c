/*
** cd.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 14 18:16:18 2015 VIctor Le
** Last update Sat May 23 13:12:22 2015 VIctor Le
*/

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "env.h"
#include "t_sh.h"
#include "t_cmd.h"
#include "set_var_env.h"
#include "epur_path.h"

/*
** Vérifie existence d'un home.
** Modifie l'environnement en conséquence.
** Set les pointeurs personnels de façon corrects.
*/
static int	cd_home(t_env *env)
{
  if (env->home == NULL)
    {
      fprintf(stderr, "cd: no home directory.\n");
      return (1);
    }
  if (chdir(env->home) == -1)
    {
      fprintf(stderr, "cd: %s\n", strerror(errno));
      return (1);
    }
  printf("Directory: %s\n", env->home);
  env->env = set_var_env(env->env, "OLDPWD", env->pwd);
  env->env = set_var_env(env->env, "PWD", env->home);
  free(env->oldpwd);
  if (env->pwd != NULL && (env->oldpwd = strdup(env->pwd)) == NULL)
    return (2);
  free(env->pwd);
  if (env->home != NULL && (env->pwd = strdup(env->home)) == NULL)
    return (2);
  epur_path(env->oldpwd);
  epur_path(env->pwd);
  return (0);
}

/*
** Vérifie l'existence d'un oldpwd.
** Modifie l'environnement en conséquence.
** Set les pointeurs personnels de façon corrects.
*/
static int	cd_oldpwd(t_env *env)
{
  char		*tmp;

  if (env->oldpwd == NULL)
    {
      fprintf(stderr, ": No such file or directory\n");
      return (1);
    }
  if (chdir(env->oldpwd) == -1)
    {
      fprintf(stderr, "cd: %s\n", strerror(errno));
      return (1);
    }
  printf("Directory: %s\n", env->oldpwd);
  env->env = set_var_env(env->env, "OLDPWD", env->pwd);
  env->env = set_var_env(env->env, "PWD", env->oldpwd);
  tmp = env->oldpwd;
  if (env->pwd != NULL && (env->oldpwd = strdup(env->pwd)) == NULL)
    return (2);
  free(env->pwd);
  if (tmp != NULL && (env->pwd = strdup(tmp)) == NULL)
    return (2);
  free(tmp);
  epur_path(env->pwd);
  epur_path(env->oldpwd);
  return (0);
}

/*
** Effectue l'expansion de ~/... avec le home, si le home eiste.
*/
static char	*path_expansion(char *dir, t_env *env)
{
  char		*path;
  int		len;

  len = strlen(dir) + 2;
  if (dir[0] == '~' && env->home != NULL)
    len += strlen(env->home);
  else if (env->pwd != NULL)
    len += strlen(env->pwd);
  if ((path = malloc(sizeof(*path) * len)) == NULL)
    return (NULL);
  if (dir[0] == '~' && env->home != NULL)
    strcpy(path, env->home);
  else
    strcpy(path, env->pwd);
  strcat(path, "/");
  if (dir[0] == '~')
    strcat(path, dir + 1);
  else
    strcat(path, dir);
  epur_path(path);
  return (path);
}

/*
** Effectue l'expansion si nécessaire.
** builtin cd d'un chemin classique.
*/
static int	classic_cd(char *dir, t_env *env)
{
  char		*path;

  if (dir[0] != '/' && (path = path_expansion(dir, env)) == NULL)
    return (0);
  else if (dir[0] == '/' && (path = strdup(dir)) == NULL)
    return (0);
  if (chdir(path) == -1)
    {
      fprintf(stderr, "%s: %s\n", dir, strerror(errno));
      free(path);
      return (1);
    }
  printf("Directory: %s\n", path);
  env->env = set_var_env(env->env, "OLDPWD", env->pwd);
  env->env = set_var_env(env->env, "PWD", path);
  free(env->oldpwd);
  if (env->pwd != NULL && (env->oldpwd = strdup(env->pwd)) == NULL)
    return (2);
  epur_path(env->oldpwd);
  free(env->pwd);
  if ((env->pwd = strdup(path)) == NULL)
    return (2);
  free(path);
  return (0);
}

int	builtin_cd(t_cmd *cmd, t_sh *sh)
{
  if (cmd->list_size == 1)
    return (cd_home(&sh->env));
  if (!strcmp(cmd->head->next->token, "-"))
    return (cd_oldpwd(&sh->env));
  return (classic_cd(cmd->head->next->token, &sh->env));
}
