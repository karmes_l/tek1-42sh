/*
** t_cmd.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:46:11 2015 VIctor Le
** Last update Fri May 22 16:19:16 2015 VIctor Le
*/

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "t_cmd.h"
#include "t_token.h"
#include "free_and_null.h"

void	t_cmd_constructor(t_cmd *cmd)
{
  cmd->next = NULL;
  cmd->tail = NULL;
  cmd->head = NULL;
  cmd->tail = NULL;
  cmd->buffer = NULL;
  cmd->list_size = 0;
  cmd->ret = 0;
  cmd->operator[0] = OP_VOID;
  cmd->operator[1] = OP_VOID;
}

int		t_cmd_append_token(t_cmd *cmd, char *token, char isword)
{
  t_token	*node;

  if (cmd == NULL || token == NULL || (node = malloc(sizeof(*node))) == NULL)
    return (0);
  t_token_constructor(node);
  node->token = token;
  node->isword = isword;
  node->prev = cmd->tail;
  if (cmd->head == NULL)
    cmd->head = node;
  if (cmd->tail != NULL)
    cmd->tail->next = node;
  cmd->tail = node;
  ++cmd->list_size;
  return (1);
}

t_token		*t_cmd_remove_token(t_cmd *cmd, t_token *token)
{
  t_token	*ret;

  ret = NULL;
  if (cmd != NULL && token != NULL)
    {
      if (token == cmd->head)
	{
	  cmd->head = t_token_destructor(token);
	  ret = cmd->head;
	}
      else if (token == cmd->tail)
	{
	  cmd->tail = cmd->tail->prev;
	  ret = t_token_destructor(token);
	}
      else
	ret = t_token_destructor(token);
      if (--cmd->list_size == 0)
	cmd->tail = NULL;
    }
  return (ret);
}

static void	t_cmd_free_buffer(char **buffer)
{
  int		i;

  if (buffer != NULL)
    {
      i = 0;
      while (buffer[i] != NULL)
	{
	  free_and_null((void **)&buffer[i]);
	  ++i;
	}
      free_and_null((void **)&buffer);
    }
}

t_cmd	*t_cmd_destructor(t_cmd *cmd)
{
  t_cmd	*ret;

  ret = NULL;
  if (cmd != NULL)
    {
      ret = cmd->next;
      while (cmd->head != NULL && cmd->list_size > 0)
	t_cmd_remove_token(cmd, cmd->head);
      if (cmd->next != NULL)
	cmd->next->prev = cmd->prev;
      if (cmd->prev != NULL)
	cmd->prev->next = cmd->next;
      if (cmd->buffer != NULL)
	t_cmd_free_buffer(cmd->buffer);
      free_and_null((void **)&cmd);
    }
  return (ret);
}
