/*
** epur_path.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 14 21:35:01 2015 VIctor Le
** Last update Sat May 16 20:21:45 2015 VIctor Le
*/

#include <stdlib.h>

static void	skip_slash(char *path, int *i, int *j)
{
  while (path[*i] == '/')
    ++(*i);
  if (*j > 0 && path[*j - 1] != '/')
    {
      path[*j] = '/';
      ++(*j);
    }
  if (*j == 0 && path[*j] == '/')
    ++(*j);
}

static int	count_dot(char *path, int i)
{
  int		dot;

  dot = 0;
  while (path[i] == '.')
    {
      ++dot;
      ++i;
    }
  return (dot);
}

static void	dot_case(char *path, int *i, int *j)
{
  int		dot;
  int		k;

  dot = count_dot(path, *i);
  *i += dot;
  if (dot == 2)
    {
      *j = (*j - 2 < 2) ? 0 : *j - 2;
      while (*j > 0 && path[*j] != '/')
	--(*j);
      ++(*j);
    }
  else if (dot > 2)
    {
      k = 0;
      while (k < dot)
	{
	  path[*j] = '.';
	  ++(*j);
	  ++k;
	}
    }
}

static void	copy_dir(char *path, int *i, int *j)
{
  while (path[*i] != '\0' && path[*i] != '/')
    {
      if (path[*i] == '.')
	dot_case(path, i, j);
      else
	{
	  path[*j] = path[*i];
	  ++(*j);
	  ++(*i);
	}
    }
}

void	epur_path(char *path)
{
  int	i;
  int	j;

  if (path != NULL)
    {
      i = 0;
      j = 0;
      while (path[i] != '\0')
	{
	  skip_slash(path, &i, &j);
	  if (path[i] == '\0')
	    break ;
	  copy_dir(path, &i, &j);
	}
      if (j > 1 && path[j - 1] == '/')
	path[j - 1] = '\0';
      else
	path[j] = '\0';
    }
}
