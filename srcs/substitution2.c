/*
** substitution2.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Wed May 20 18:21:43 2015 Aurélien Metz
** Last update Sun May 24 15:05:07 2015 VIctor Le
*/

#include "substitution.h"

int		char_in_str(const char c, const char *s)
{
  unsigned int	i;

  i = 0;
  while (s && s[i])
    if (c == s[i++])
      return (1);
  return (0);
}

int		str_in_str(const char *s, const char *ref)
{
  unsigned int	i;

  i = 0;
  while (s[i])
    if (char_in_str(s[i++], ref) == 0)
      return (0);
  return (1);
}

char		*find_var_env(char ** const env, const char *var)
{
  unsigned int	i;

  if (env == NULL || var == NULL)
    return (NULL);
  i = 0;
  while (env[i])
    {
      if (strncmp(var, env[i], strlen(var)) == 0
	  && env[i][strlen(var)] == '=')
	return (env[i] + strlen(var) + 1);
      ++i;
    }
  return (NULL);
}
