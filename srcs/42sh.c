/*
** 42sh.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh/srcs
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Fri Apr 10 18:07:16 2015 Aurélien Metz
** Last update Fri Jun  5 21:34:19 2015 VIctor Le
*/

#include <unistd.h>
#include <stdlib.h>
#include "read_signal.h"
#include "gnl.h"
#include "parser.h"
#include "t_list.h"
#include "t_sh.h"
#include "str_to_wdtab.h"
#include "lexer.h"
#include "exec_cmd.h"
#include "is_correct_quoted.h"
#include "termcaps.h"
#include "t_list_hist.h"
#include "free_and_null.h"

static char	**read_line(t_sh *sh)
{
  char		*s;
  char		**wdtab_cmd;

  read_signal(PROC_DAD);
  if ((s = read_cmd(sh)) == NULL || !is_correct_quoted(s))
    {
      free_and_null((void **)&s);
      return (NULL);
    }
  if (s[0] == '\n' || s[0] == '\0')
    {
      if (s[0] == '\0')
	{
	  write(1, "exit\n", 5);
	  sh->ask_exit = 1;
	}
      free(s);
      return (NULL);
    }
  if ((wdtab_cmd = str_to_wdtab(s, ";")) == NULL)
    return (NULL);
  free(s);
  return (wdtab_cmd);
}

static int	lines_exec(t_sh *sh, char **wdtab_cmd)
{
  t_list	list;
  int		i;

  t_list_constructor(&list);
  i = 0;
  while (wdtab_cmd[i] != NULL)
    {
      if (!parser(wdtab_cmd[i], &list))
	{
	  free_wdtab(wdtab_cmd);
	  t_list_destructor(&list);
	  return (EXEC_ERROR);
	}
      if (lexer(&list))
	if ((exec_cmd(list.head, sh)) == EXEC_PIPE)
	  {
	    t_list_destructor(&list);
	    free_wdtab(wdtab_cmd);
	    return (EXEC_PIPE);
	  }
      t_list_destructor(&list);
      ++i;
    }
  free_wdtab(wdtab_cmd);
  return (EXEC_OK);
}

int		main(int ac, char **av, char **env)
{
  t_sh		sh;
  char		**wdtab_cmd;

  (void)ac;
  (void)av;
  t_sh_constructor(&sh);
  if (env[0] != NULL && to_env(env, &sh.env) == -2)
    return (sh.cmd_return);
  while (!sh.ask_exit)
    {
      if ((wdtab_cmd = read_line(&sh)) == NULL)
	continue ;
      if (lines_exec(&sh, wdtab_cmd) == EXEC_PIPE)
	{
	  t_sh_destructor(&sh);
	  return (sh.cmd_return);
	}
    }
  t_sh_destructor(&sh);
  return (sh.cmd_return);
}
