/*
** builtin_env_tab.c for builtin env tab in /home/lecavo_e/PROJET/42sh/PSU_2014_42sh
**
** Made by enora lecavorzin-metallinos
** Login   <lecavo_e@epitech.net>
**
** Started on  Wed May 20 14:03:14 2015 enora lecavorzin-metallinos
** Last update Fri May 22 12:04:47 2015 VIctor Le
*/

#include <unistd.h>

void	my_putchar(char c)
{
  (void)write(1, &c, 1);
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_putchar(str[i]);
      i++;
    }
}
