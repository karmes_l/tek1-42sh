/*
** builtin_exit.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 10:24:11 2015 VIctor Le
** Last update Sat May 16 16:04:19 2015 VIctor Le
*/

#include <stdio.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "get_nbr.h"

static int	exit_error(void)
{
  printf("Usage: exit [n]\n");
  return (1);
}

int	builtin_exit(t_cmd *cmd, t_sh *sh)
{
  if (cmd->list_size > 2)
    return (exit_error());
  sh->ask_exit = 1;
  if (cmd->list_size == 2)
    return (get_nbr(cmd->tail->token));
  return (0);
}
