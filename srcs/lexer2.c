/*
** lexer2.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh/workspace/parser
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Tue May 19 16:03:29 2015 Aurélien Metz
** Last update Sun May 24 15:26:30 2015 VIctor Le
*/

#include "lexer.h"

int	check_token(const t_token *token, char *output, char *input)
{
  if (token->token[0] == LEFT_RED && *input)
    return (fprintf(stderr, CMD_AMB, INPUT));
  else if (token->token[0] == LEFT_RED)
    ++*input;
  else if (token->token[0] == RIGHT_RED && *output)
    return (fprintf(stderr, CMD_AMB, OUTPUT));
  else if (token->token[0] == RIGHT_RED)
    ++*output;
  if (token->next == NULL || token->next->isword == 0)
    return (fprintf(stderr, MISSING_FILE));
  if ((token->prev == NULL || token->prev->isword == 0)
      && (find_word(token->next->next) == -1
	  || token->next->next->isword == 0))
    return (fprintf(stderr, CMD_NULL));
  if (strcmp(token->token, "<") == 0
      && access(token->next->token, F_OK) == -1)
    return (fprintf(stderr, DOESNT_EXIST, token->next->token));
  return (0);
}
