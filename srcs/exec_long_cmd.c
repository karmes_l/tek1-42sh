/*
** exec_long_cmd.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:34:57 2015 VIctor Le
** Last update Fri May 22 12:18:30 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "exec_cmd.h"
#include "exec_simple_cmd.h"
#include "exec_pipe.h"

/*
** Indique si la cmd fait partie d'une cmd listée.
** Ne fonctionne pas sur la 1ere cmd d'une cmd listée.
*/
static int	is_in_or_and_cmd(t_cmd *cmd)
{
  if (cmd == NULL)
    return (0);
  if (cmd->operator[0] == OP_OR || cmd->operator[0] == OP_AND)
    return (1);
  return (0);
}

/*
** Permet de sauter toutes les commandes incluses dans un pipe.
** Renvoie un pointeur sur la cmd après la dernière instruction lié à un pipe.
*/
static t_cmd	*jump_to_next_cmd(t_cmd *cmd)
{
  cmd = cmd->next;
  while (cmd && cmd->operator[0] == OP_PIPE)
    cmd = cmd->next;
  return (cmd);
}

/*
** Indique si la cmd donnée est la première de la cmd listée.
** Retourne 1 si oui, 0 sinon.
*/
static int	is_first_cmd_in_compound(t_cmd *cmd)
{
  if (cmd->operator[0] == OP_VOID
      || cmd->operator[0] == OP_SEMICOLON
      || cmd->operator[0] == OP_BG)
    return (1);
  return (0);
}

/*
** Vérifie que la cmd précédente corresponde aux conditions d'éxécution.
** Renvoie EXEC_END, si les conditions ne conviennent pas, sinon
** Retour de exec_simple_cmd().
*/
static int	exec_long_cmd_or_and(t_cmd *cmd, t_sh *sh)
{
  if (((cmd->operator[0] == OP_OR) && cmd->prev->ret == 0)
      || (!(cmd->operator[0] == OP_OR) && cmd->prev->ret != 0))
    return (EXEC_END);
  return (exec_simple_cmd(cmd, sh, 0));
}

int	exec_long_cmd(t_cmd *cmd, t_sh *sh)
{
  int	ret;

  while (cmd != NULL && (is_first_cmd_in_compound(cmd) || is_in_or_and_cmd(cmd)
			 || cmd->operator[1] == OP_PIPE))
    {
      if (cmd->operator[1] == OP_PIPE)
	{
	  if ((ret = exec_pipe(cmd, sh)) != EXEC_OK)
	    return (ret);
	  cmd = jump_to_next_cmd(cmd);
	}
      else if (is_first_cmd_in_compound(cmd))
	{
	  if ((ret = exec_simple_cmd(cmd, sh, 0)) != EXEC_OK)
	    return (ret);
	  cmd = cmd->next;
	}
      else if (is_in_or_and_cmd(cmd))
	{
	  if ((ret = exec_long_cmd_or_and(cmd, sh)) != EXEC_OK)
	    return (ret);
	  cmd = cmd->next;
	}
    }
  return (EXEC_OK);
}
