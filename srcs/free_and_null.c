/*
** free_and_null.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:30:48 2015 VIctor Le
** Last update Tue May  5 08:36:51 2015 VIctor Le
*/

#include <stdlib.h>

void	free_and_null(void **ptr)
{
  if (ptr != NULL && *ptr != NULL)
    {
      free(*ptr);
      *ptr = NULL;
    }
}
