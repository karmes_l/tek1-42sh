/*
** redirection.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May  6 15:33:39 2015 lionel karmes
** Last update Sun May 24 16:31:12 2015 lionel karmes
*/

#include "redirection.h"

void	close_fd(int fd)
{
  if (fd != -1)
    (void)close(fd);
}

/*
** Check les redirections et modifie t_redir
** Tout en créant les fichiers
** Si fail de open(), RETURN : errno (ENOENT dans la plupart des cas)
*/
static int	update_redir(t_redir *redir, char *chevron, char *path)
{
  int		ret;
  int		(*chev[4])(t_redir *, char *, char *);
  int		i;

  chev[0] = &chevron_right;
  chev[1] = &chevron_double_right;
  chev[2] = &chevron_left;
  chev[3] = &chevron_double_left;
  i = 0;
  while ((ret = chev[i](redir, chevron, path)) == 2 && i < 4)
    ++i;
  if (ret == 2)
    return (FAIL);
  if (ret == FAIL)
    t_redir_destructor(redir);
  return (ret);
}

static int	fildes_double_left(char **buffer)
{
  int		fildes[2];
  int		i;

  if (pipe(fildes) == -1)
    return (FAIL);
  i = 0;
  while (buffer[i])
    {
      (void)write(fildes[1], buffer[i], strlen(buffer[i]));
      ++i;
    }
  close_fd(fildes[1]);
  if (dup2(fildes[0], 0) == -1)
    {
      close_fd(fildes[0]);
      return (FAIL);
    }
  close_fd(fildes[0]);
  return (OK);
}

/*
** Utilisation de dup2, redirige le file descriptor vers un autre
** Va rediriger les entrée de fd et les sortie de fd
*/
static int	dup_redir(t_redir *redir, t_cmd *cmd)
{
  int		fd;

  fd = -1;
  if (redir->entry &&
      ((fd = open_file(redir->entry)) == -1 || (dup2(fd, 0) == -1)))
    {
      close_fd(fd);
      return (FAIL);
    }
  else if (cmd->buffer)
    {
      if (!fildes_double_left(cmd->buffer))
	return (FAIL);
    }
  close_fd(fd);
  if (redir->sorty &&
      ((fd = open_file(redir->sorty)) == -1 || (dup2(fd, 1) == -1)))
    {
      close_fd(fd);
      return (FAIL);
    }
  close_fd(fd);
  return (OK);
}

int		handle_redirections(t_cmd *cmd)
{
  t_redir	*redir;
  t_token	*tmp;
  int		ret;

  if (!(redir = t_redir_constructor()))
    return (FAIL);
  tmp = cmd->head;
  while (tmp)
    {
      if (!(tmp->isword))
	{
	  if (!(ret = update_redir(redir, tmp->token, tmp->next->token))
	      || ret == ENOENT)
	    return (ret);
	  tmp = t_cmd_remove_token(cmd, tmp);
	  tmp = t_cmd_remove_token(cmd, tmp);
	  continue ;
	}
      tmp = tmp->next;
    }
  if (!dup_redir(redir, cmd))
    return (FAIL);
  t_redir_destructor(redir);
  return (OK);
}
