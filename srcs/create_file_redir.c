/*
** create_file_redir.c for 42sh in /home/le_l/workspace/42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 22 12:29:26 2015 VIctor Le
** Last update Thu Jun  4 17:55:44 2015 VIctor Le
*/

#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include "t_cmd.h"
#include "t_token.h"
#include "redirection.h"

int		create_file(char *filename, int append)
{
  int		fd;
  int		ret;

  ret = 1;
  if (!append)
    {
      if ((fd = open(filename, O_CREAT, S_IRUSR | S_IWUSR
		     | S_IRGRP | S_IROTH)) == -1)
	ret = 0;
    }
  else if ((fd = open(filename, O_APPEND)) == -1)
    ret = 0;
  if (!ret)
    fprintf(stderr, "Cannot succeed to create %s file\n", filename);
  close_fd(fd);
  return (ret);
}

int		create_file_redir(t_cmd *cmd)
{
  t_token	*token;

  while (cmd != NULL)
    {
      token = cmd->head;
      while (token != NULL)
	{
	  if (!strcmp(token->token, ">") && token->next && token->next->isword)
	    {
	      if (!create_file(token->next->token, 0))
		return (0);
	      token = token->next;
	    }
	  else if (!strcmp(token->token, ">>") && token->next
		   && token->next->isword)
	    {
	      if (!create_file(token->next->token, 1))
		return (0);
	      token = token->next;
	    }
	  token = token->next;
	}
      cmd = cmd->next;
    }
  return (1);
}
