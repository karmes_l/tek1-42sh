/*
** metachar_handler2.c for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May  8 19:53:04 2015 VIctor Le
** Last update Sat May  9 15:08:59 2015 VIctor Le
*/

#include "metachar_handler.h"
#include "t_str.h"
#include "t_list.h"
#include "t_op.h"

int	pipe_handler(char *s, int *i, t_str *str, t_list *list)
{
  if (!t_str_append_char(str, s[*i]))
    return (META_FAIL);
  ++(*i);
  if (s[*i] == '|')
    {
      if (!t_str_append_char(str, s[*i]))
	return (META_FAIL);
      ++(*i);
    }
  if (!push_back_token(str, list->tail, 0) || !operator_handler(list))
    return (META_FAIL);
  return (META_OK);
}

int	ampersand_handler(char *s, int *i, t_str *str, t_list *list)
{
  if (!t_str_append_char(str, s[*i]))
    return (META_FAIL);
  ++(*i);
  if (s[*i] == '&')
    {
      if (!t_str_append_char(str, s[*i]))
	return (META_FAIL);
      ++(*i);
    }
  if (!push_back_token(str, list->tail, 0) || !operator_handler(list))
    return (META_FAIL);
  return (META_OK);
}

int	semicolon_handler(char *s, int *i, t_str *str, t_list *list)
{
  if (!t_str_append_char(str, s[*i]))
    return (META_FAIL);
  ++(*i);
  if (!push_back_token(str, list->tail, 0) || !operator_handler(list))
    return (META_FAIL);
  return (META_OK);
}

int	open_chevron_handler(char *s, int *i, t_str *str, t_list *list)
{
  if (!t_str_append_char(str, s[*i]))
    return (META_FAIL);
  ++(*i);
  if (s[*i] == '<')
    {
      if (!t_str_append_char(str, s[*i]))
	return (META_FAIL);
      ++(*i);
    }
  if (!push_back_token(str, list->tail, 0) || !operator_handler(list))
    return (META_FAIL);
  return (META_OK);
}

int	close_chevron_handler(char *s, int *i, t_str *str, t_list *list)
{
  if (!t_str_append_char(str, s[*i]))
    return (META_FAIL);
  ++(*i);
  if (s[*i] == '>')
    {
      if (!t_str_append_char(str, s[*i]))
	return (META_FAIL);
      ++(*i);
    }
  if (!push_back_token(str, list->tail, 0) || !operator_handler(list))
    return (META_FAIL);
  return (META_OK);
}
