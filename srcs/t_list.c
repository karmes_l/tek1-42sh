/*
** t_list.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:22:21 2015 Victor Le
** Last update Fri May 15 09:03:28 2015 Aurélien Metz
*/

#include <stdlib.h>
#include "t_list.h"
#include "t_cmd.h"
#include "free_and_null.h"

void	t_list_constructor(t_list *list)
{
  list->head = NULL;
  list->tail = NULL;
  list->list_size = 0;
}

int	t_list_append_empty_cmd(t_list *list)
{
  t_cmd	*node;

  if (list == NULL || (node = malloc(sizeof(*node))) == NULL)
    return (0);
  t_cmd_constructor(node);
  node->prev = list->tail;
  if (list->head == NULL)
    list->head = node;
  if (list->tail != NULL)
    list->tail->next = node;
  list->tail = node;
  ++list->list_size;
  return (1);
}

void	t_list_remove_cmd(t_list *list, t_cmd *cmd)
{
  if (list != NULL && cmd != NULL)
    {
      if (cmd == list->head)
	list->head = t_cmd_destructor(cmd);
      else if (cmd == list->tail)
	{
	  list->tail = list->tail->prev;
	  (void)t_cmd_destructor(cmd);
	}
      else
	(void)t_cmd_destructor(cmd);
      if (--list->list_size == 0)
	list->tail = NULL;
    }
}

void	t_list_destructor(t_list *list)
{
  if (list != NULL)
    {
      while (list->head != NULL && list->list_size > 0)
	t_list_remove_cmd(list, list->head);
    }
}
