/*
** open_file.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May  8 16:52:59 2015 lionel karmes
** Last update Sun May 24 15:14:02 2015 VIctor Le
*/

#include "redirection.h"
#include "t_file.h"

int	open_file(t_file *file)
{
  return (open(file->path, file->flags, 00644));
}

int	chevron_right(t_redir *redir, char *chevron, char *path)
{
  int	fd;

  if (!(strcmp(CHEV_R, chevron)))
    {
      t_file_destructor(redir->sorty);
      if (!(redir->sorty = t_file_constructor(strdup(path),
					      O_TRUNC | O_WRONLY | O_CREAT)))
	return (FAIL);
      if ((fd = open_file(redir->sorty)) == -1)
	return (ENOENT);
      close_fd(fd);
      return (OK);
    }
  return (2);
}

int	chevron_double_right(t_redir *redir, char *chevron, char *path)
{
  int	fd;

  if (!(strcmp(CHEV_DR, chevron)))
    {
      t_file_destructor(redir->sorty);
      if (!(redir->sorty = t_file_constructor(strdup(path),
					      O_APPEND | O_WRONLY | O_CREAT)))
	return (FAIL);
      if ((fd = open_file(redir->sorty)) == -1)
	return (ENOENT);
      close_fd(fd);
      return (OK);
    }
  return (2);
}

int	chevron_left(t_redir *redir, char *chevron, char *path)
{
  int	fd;

  if (!(strcmp(CHEV_L, chevron)))
    {
      t_file_destructor(redir->entry);
      if (!(redir->entry = t_file_constructor(strdup(path), O_RDONLY)))
	return (FAIL);
      if ((fd = open_file(redir->entry)) == -1)
	return (ENOENT);
      close_fd(fd);
      return (OK);
    }
  return (2);
}

int	chevron_double_left(t_redir *redir, char *chevron, char *path)
{
  (void)path;
  if (!(strcmp(CHEV_DL, chevron)) || !(strcmp(CHEV_TL, chevron)))
    {
      t_file_destructor(redir->entry);
      return (OK);
    }
  return (2);
}
