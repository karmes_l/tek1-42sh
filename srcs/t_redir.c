/*
** t_redir.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May  6 16:10:35 2015 lionel karmes
** Last update Sun May 24 15:03:42 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_redir.h"
#include "free_and_null.h"

t_redir		*t_redir_constructor(void)
{
  t_redir	*redir;

  if (!(redir = malloc(sizeof(t_redir))))
    return (NULL);
  redir->entry = NULL;
  redir->sorty = NULL;
  return (redir);
}

void	t_redir_destructor(t_redir *redir)
{
  if (redir)
    {
      t_file_destructor(redir->entry);
      t_file_destructor(redir->sorty);
      free_and_null((void **)&redir);
    }
}
