/*
** metachar_handler.c for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May  8 19:31:04 2015 VIctor Le
** Last update Sat May 16 07:13:33 2015 VIctor Le
*/

#include <stdlib.h>
#include "metachar_handler.h"
#include "t_str.h"
#include "t_list.h"
#include "t_cmd.h"

/*
** Renvoie un index pour être utilisée avec un tab de pointeur sur fonction
** -1 si le carac n'est pas un meta.
*/
static int	is_metachar(char c)
{
  char		*meta;
  int		i;

  meta = METACHARS;
  i = 0;
  while (i < NB_META && meta[i] != '\0' && c != meta[i])
    ++i;
  if (i < NB_META && meta[i] != '\0')
    return (i);
  return (-1);
}

/*
** Initialise le tableau de popinteur sur fonction de type t_meta.
*/
static void	t_meta_fptr_init(t_meta *fptr)
{
  fptr[0] = &pipe_handler;
  fptr[1] = &ampersand_handler;
  fptr[2] = &semicolon_handler;
  fptr[3] = &open_chevron_handler;
  fptr[4] = &close_chevron_handler;
  fptr[5] = &blank_handler;
  fptr[6] = &blank_handler;
  fptr[7] = &newline_handler;
}

int	push_back_token(t_str *str, t_cmd *last_cmd, int isword)
{
  char	*tmp;

  if (str->size == 0 || str->head == NULL)
    return (1);
  if (!(tmp = t_str_get_str(str)))
    return (0);
  if (!t_cmd_append_token(last_cmd, tmp, isword))
    return (0);
  t_str_destructor(str);
  t_str_constructor(str);
  return (1);
}

int		metachar_handler(char *s, int *i, t_str *str, t_list *list)
{
  t_meta	fptr[NB_META];
  int		i_meta;

  if ((i_meta = is_metachar(s[*i])) < 0)
    return (NOT_META);
  if (!push_back_token(str, list->tail, 1))
    return (META_FAIL);
  t_meta_fptr_init(fptr);
  if (fptr[i_meta](s, i, str, list) == META_FAIL)
    return (META_FAIL);
  return (META_OK);
}
