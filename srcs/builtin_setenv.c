/*
** setenv.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/setenv
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 11:31:45 2015 VIctor Le
** Last update Sun Jun  7 22:51:45 2015 lionel karmes
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "set_var_env.h"
#include "free_and_null.h"
#include "str_to_wdtab.h"
#include "search_var_env.h"
#include "builtins.h"

static int	setenv_error(void)
{
  printf("Usage: setenv [var_name] [value]\n");
  return (2);
}

static int	update_env_ptrs(t_env *env, char *var_name)
{
  char		*tmp;

  if (!strcmp(var_name, "HOME"))
    {
      free_and_null((void **)&env->home);
      if ((env->home = search_var_env(env->env, var_name, NULL)) == NULL)
	return (0);
      env->home = strdup(env->home + 5);
    }
  else if (!strcmp(var_name, "PATH"))
    {
      free_wdtab(env->path);
      if ((tmp = search_var_env(env->env, var_name, NULL)) == NULL)
	return (0);
      env->path = str_to_wdtab(tmp + 5, ":");
    }
  return (1);
}

int	builtin_setenv(t_cmd *cmd, t_sh *sh)
{
  char	*args[2];

  if (cmd == NULL || sh == NULL)
    return (1);
  if (cmd->list_size > 3)
    return (setenv_error());
  if (cmd->list_size == 1)
    {
      print_env(sh->env.env);
      return (0);
    }
  args[0] = cmd->head->next->token;
  args[1] = (cmd->head->next->next == NULL) ? "" :
    cmd->head->next->next->token;
  sh->env.env = set_var_env(sh->env.env, args[0], args[1]);
  if (sh->env.env == NULL)
    return (1);
  if (!update_env_ptrs(&sh->env, args[0]))
    return (1);
  return (0);
}
