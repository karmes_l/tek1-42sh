/*
** builtin_echo.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 08:38:00 2015 VIctor Le
** Last update Sat May 16 21:51:19 2015 VIctor Le
*/

#include <unistd.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "echo.h"

int	builtin_echo(t_cmd *cmd, t_sh *sh)
{
  (void)sh;
  if (cmd->list_size == 1)
    (void)write(1, "\n", 1);
  else if (cmd->list_size > 1)
    echo(cmd->head->next);
  return (0);
}
