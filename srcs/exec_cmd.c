/*
** exec_cmd.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:33:03 2015 VIctor Le
** Last update Fri May 22 12:44:13 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "exec_long_cmd.h"
#include "exec_simple_cmd.h"
#include "exec_cmd.h"
#include "create_file_redir.h"

int	exec_cmd(t_cmd *cur_cmd, t_sh *sh)
{
  if (cur_cmd->head != NULL)
    {
      if (!create_file_redir(cur_cmd))
	return (0);
      if (cur_cmd->operator[1] == OP_PIPE
	  || cur_cmd->operator[1] == OP_AND
	  || cur_cmd->operator[1] == OP_OR)
	return (exec_long_cmd(cur_cmd, sh));
      return (exec_simple_cmd(cur_cmd, sh, 0));
    }
  return (EXEC_OK);
}
