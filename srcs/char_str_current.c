/*
** char_str_current.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Sat May 16 11:10:40 2015 lionel karmes
** Last update Thu May 21 12:55:22 2015 lionel karmes
*/

#include <stdlib.h>
#include "termcaps.h"

int		add_char_to_str(t_str *str, char c, int fd_tty)
{
  if (c == '\n')
    {
      if (!t_str_append_char2(str, str->tail, c))
	return (FAIL);
      write(fd_tty, &c, 1);
    }
  else
    {
      pos_cursor_current(fd_tty, str, str->head);
      if (!t_str_append_char2(str, str->current, c))
	return (FAIL);
      update_line(fd_tty, str);
    }
  return (TERM_OK);
}

void		char_destroy_prev(t_str *str, int fd_tty)
{
  t_char	*tmp;

  if (str->current != str->head)
    {
      pos_cursor_current(fd_tty, str, str->head);
      tmp = str->current->prev->prev;
      if (str->current->prev->prev != NULL)
	str->current->prev->prev->next = str->current;
      else
	str->head = str->current;
      t_char_destructor(str->current->prev);
      str->current->prev = tmp;
      --str->size;
      update_line(fd_tty, str);
    }
}

void		char_destroy_current(t_str *str, int fd_tty)
{
  t_char	*tmp;

  if (str->current != str->tail)
    {
      pos_cursor_current(fd_tty, str, str->head);
      tmp = str->current->next;
      if (str->current->prev != NULL)
	str->current->prev->next = str->current->next;
      else
	str->head = tmp;
      str->current->next->prev = str->current->prev;
      t_char_destructor(str->current);
      str->current = tmp;
      --str->size;
      update_line(fd_tty, str);
    }
}

void		chars_destroy(t_str *str, t_char *begin, t_char *end, int fd)
{
  t_char	*tmp;

  pos_cursor_current(fd, str, str->head);
  str->current = begin;
  while (str->current != end && str->current)
    {
      if (str->current != str->tail)
	{
	  tmp = str->current->next;
	  if (str->current->prev != NULL)
	    str->current->prev->next = str->current->next;
	  else
	    str->head = tmp;
	  str->current->next->prev = str->current->prev;
	  t_char_destructor(str->current);
	  str->current = tmp;
	  --str->size;
	}
    }
  update_line(fd, str);
}

void		move_current_char(t_str *str, t_char *current, int fd)
{
  pos_cursor_current(fd, str, current);
  str->current = current;
}
