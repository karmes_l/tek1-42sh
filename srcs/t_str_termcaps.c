/*
** t_str2.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri May 15 14:27:36 2015 lionel karmes
** Last update Sat May 16 12:34:56 2015 lionel karmes
*/

#include <stdlib.h>
#include <string.h>
#include "t_str.h"

int		t_str_append_char2(t_str *str, t_char *current, char c)
{
  t_char	*node;

  if ((node = malloc(sizeof(*node))) == NULL)
    return (0);
  t_char_constructor(node);
  node->c = c;
  node->next = current;
  node->prev = current->prev;
  if (current->prev == NULL)
    str->head = node;
  else
    current->prev->next = node;
  current->prev = node;
  ++str->size;
  return (1);
}

int		t_str_dup(t_str *dest, t_str *src)
{
  t_char	*current;
  t_char	*tmp;

  if (!src || !src->tail || !src->head)
    return (0);
  t_str_constructor(dest);
  if (!t_str_append_char(dest, src->tail->c))
    return (0);
  dest->current = dest->tail;
  current = dest->tail;
  tmp = src->head;
  while (tmp != src->tail)
    {
      if (!t_str_append_char2(dest, current, tmp->c))
	return (0);
      if (src->current == tmp)
	dest->current = dest->tail->prev;
      tmp = tmp->next;
    }
  return (1);
}

int		char_to_str(t_str *dest, char *cmd)
{
  t_char	*current;
  unsigned int	i;

  if (!cmd)
    return (0);
  t_str_constructor(dest);
  if (!t_str_append_char(dest, '\0'))
    return (0);
  current = dest->tail;
  i = 0;
  while (i < strlen(cmd))
    {
      if (!t_str_append_char2(dest, current, cmd[i]))
	return (0);
      ++i;
    }
  return (1);
}
