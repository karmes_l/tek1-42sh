/*
** quote_handler.c for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May  8 21:11:03 2015 VIctor Le
** Last update Fri May 22 14:18:03 2015 VIctor Le
*/

#include <stdio.h>
#include <unistd.h>
#include "t_str.h"
#include "parser.h"
#include "free_and_null.h"
#include "quote_handler.h"
#include "gnl.h"

int	quote_handler(char **s, int *i, t_str *str)
{
  char	quote;

  quote = (*s)[*i];
  if (quote != '\'' && quote != '"')
    return (NOT_QUOTE);
  ++(*i);
  while ((*s)[*i] != '\0' && (*s)[*i] != quote)
    {
      if (!t_str_append_char(str, (*s)[*i]))
	return (QUOTE_FAIL);
      ++(*i);
    }
  return (QUOTE_OK);
}
