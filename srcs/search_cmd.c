/*
** search_cmd.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/search_cmd
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Sun May 10 13:00:22 2015 lionel karmes
** Last update Fri May 22 11:34:06 2015 VIctor Le
*/

#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include "free_and_null.h"

static char	*concat(char *arg1, char *separator, char *arg2)
{
  char		*str;

  if (!(str = malloc(sizeof(char) *
		     (strlen(arg1) + strlen(separator) + strlen(arg2) + 1))))
    return (NULL);
  strcpy(str, arg1);
  strcat(str, separator);
  strcat(str, arg2);
  return (str);
}

static char	*valid_file(char *path)
{
  struct stat	sb;

  if (!path || stat(path, &sb) == -1 || sb.st_mode & S_IFDIR)
    {
      fprintf(stderr, "%s: Command not found\n", path);
      free_and_null((void **)&path);
      return (NULL);
    }
  return (path);
}

char		*search_cmd(char **path, char *bin)
{
  int		i;
  char		*path_name;

  if (access(bin, F_OK) == 0)
    {
      if (access(bin, X_OK) == 0)
	return (valid_file(strdup(bin)));
      fprintf(stderr, "%s: Permission Denied\n", bin);
    }
  i = 0;
  if (bin[0] != '/' && path != NULL)
    while (path[i] != NULL)
      {
	if (!(path_name = concat(path[i], "/", bin)))
	  return (NULL);
	if (access(path_name, F_OK) == 0)
	  return (valid_file(path_name));
	free_and_null((void **)&path_name);
	++i;
      }
  fprintf(stderr, "%s: Command not found\n", bin);
  return (NULL);
}
