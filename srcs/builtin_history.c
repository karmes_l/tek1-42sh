/*
** builtin_history.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/history
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May 20 09:55:57 2015 lionel karmes
** Last update Thu May 21 14:10:04 2015 lionel karmes
*/

#include <stdlib.h>
#include <stdio.h>
#include "builtins.h"
#include "t_cmd.h"
#include "t_list_hist.h"
#include "get_nbr.h"

static void	aff_history(int events, t_list_hist *list_hist, int flag)
{
  t_hist	*tmp;
  int		i;
  int		limit;

  tmp = (flag & HIST_F_R) ? list_hist->tail : list_hist->head;
  limit = (events < 0) ? list_hist->list_size : events;
  i = 0;
  while (i < limit && tmp)
    {
      if (!(flag & HIST_F_H))
	{
	  if (flag & HIST_F_R)
	    printf("%6d\t", list_hist->id_current - i);
	  else
	    printf("%6d\t", list_hist->id_current - list_hist->list_size
		   + i + 1);
	}
      printf("%s\n", tmp->cmd);
      if (flag & HIST_F_R)
	tmp = tmp->prev;
      else
	tmp = tmp->next;
      ++i;
    }
}

static int	print_error(char *str)
{
  fprintf(stderr, "%s\n", str);
  return (0);
}

static int	get_opt(char *arg, int *flag)
{
  int		i;
  int		j;
  char		*opt;
  int		opt_value[3];

  opt = HIST_OPTIONS;
  opt_value[0] = HIST_F_C;
  opt_value[1] = HIST_F_H;
  opt_value[2] = HIST_F_R;
  i = 1;
  while (arg[i] != '\0')
    {
      j = 0;
      while (opt[j] != '\0' && opt[j] != arg[i])
	++j;
      if (opt[j] == '\0')
	return (print_error("Usage: history [-chrSLMT] [# number of events]."));
      *flag |= opt_value[j];
      ++i;
    }
  if (i == 1)
    return (print_error("Usage: history [-chrSLMT] [# number of events]."));
  return (1);
}

static int	load_option_history(t_cmd *cmd, int *flag, int *events)
{
  t_token	*tmp;

  tmp = cmd->head->next;
  while (tmp != NULL)
    {
      if (tmp->token[0] == '-')
	{
	  if (!get_opt(tmp->token, flag))
	    return (0);
	}
      else
	{
	  if (!is_nbr(tmp->token))
	    return (print_error("history: Badly formed number."));
	  *events = get_nbr(tmp->token);
	}
      tmp = tmp->next;
    }
  return (1);
}

int	builtin_history(t_cmd *cmd, t_sh *sh)
{
  int	flag;
  int	events;

  if (cmd == NULL || sh == NULL)
    return (1);
  if (cmd->list_size > 3)
    {
      print_error("history: Too many arguments");
      return (1);
    }
  flag = 0;
  events = -1;
  if (!load_option_history(cmd, &flag, &events))
    return (1);
  if (events == 0)
    events = 1;
  if (flag & HIST_F_C)
    t_list_hist_destructor(&sh->hist);
  aff_history(events, &sh->hist, flag);
  return (0);
}
