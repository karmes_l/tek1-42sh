/*
** create_var_env.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 14 23:35:50 2015 VIctor Le
** Last update Sat May 23 11:57:30 2015 VIctor Le
*/

#include <string.h>
#include <stdlib.h>
#include "get_nb_var.h"

static void	dup_env(char **env, char **new_env, int *i)
{
  if (env != NULL)
    {
      *i = 0;
      while (env[*i] != NULL)
	{
	  new_env[*i] = env[*i];
	  ++(*i);
	}
      free(env);
    }
}

char	**create_var_env(char **env, char *new_var, char *value)
{
  char	**new_env;
  int	size;
  int	i;

  if (new_var == NULL || value == NULL)
    return (NULL);
  size = get_nb_var(env);
  if ((new_env = malloc(sizeof(*new_env) * (size + 2))) == NULL)
    return (NULL);
  i = 0;
  dup_env(env, new_env, &i);
  if ((new_env[i] = malloc(sizeof(**new_env)
			   * (strlen(new_var) + strlen(value) + 2))) == NULL)
    return (NULL);
  strcpy(new_env[i], new_var);
  strcat(new_env[i], "=");
  strcat(new_env[i], value);
  new_env[++i] = NULL;
  return (new_env);
}
