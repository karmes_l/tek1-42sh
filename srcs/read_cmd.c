/*
** read_cmd.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Thu May 14 12:44:32 2015 lionel karmes
** Last update Sun Jun  7 15:43:30 2015 VIctor Le
*/

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "termcaps.h"
#include "t_str.h"
#include "gnl.h"
#include "history.h"
#include "substitution.h"

/*
** Si char_printable : Ajout à t_str *str en fonction de la position
**	ET affichage de la touche frappée
** Si DEL : destruction du carractère avant le curseur (si possible)
*/
static int	is_read_one_char(t_str *str, char c[4],
				 int fd_tty, char *prompt)
{
  if (c[1] == 0)
    {
      if (c[0] == 4)
	{
	  char_destroy_current(str, fd_tty);
	  return (CTRL_D);
	}
      if (char_printable(c[0]))
	{
	  if (!add_char_to_str(str, c[0], fd_tty))
	    return (FAIL);
	}
      else if (c[0] == DEL)
	char_destroy_prev(str, fd_tty);
      else if (c[0] == CTRL_L)
	clear_tty(fd_tty, str, prompt);
      else if (c[0] == CTRL_U)
	chars_destroy(str, str->head, str->tail, fd_tty);
      else if (c[0] == CTRL_K)
	chars_destroy(str, str->current, str->tail, fd_tty);
      else if (c[0] == CTRL_W)
	chars_destroy(str, str->head, str->current, fd_tty);
      return (TERM_OK);
    }
  return (-1);
}

/*
** Modifie t_str *str suivant la touche c[4] frappée
** Cf. commentaire sur is_read_one_char()
** Si SUPPR : destruction du carractère sous le curseur (si possible)
** SI BEGIN (touche en dessous SUPPR) : Deplacement du curseur au debut
** SI FIN (touche en dessous SUPPR) : Deplacement du curseur à la fin
*/
static int	str_handler(t_str *str, t_sh *sh, char c[4], int fd_tty)
{
  int		ret;

  if ((ret = is_read_one_char(str, c, fd_tty, sh->prompt)) != -1)
    return (ret);
  else if ((ret = is_arrow(c)))
    {
      if (!(arrow_handler(str, &sh->hist.it, ret, fd_tty)))
	return (FAIL);
    }
  else if (is_suppr(c))
    char_destroy_current(str, fd_tty);
  else if (is_begin(c))
    move_current_char(str, str->head, fd_tty);
  else if (is_end(c))
    move_current_char(str, str->tail, fd_tty);
  return (TERM_OK);
}

/*
** Boucle récupérant chaque frappe de touche
** Initialisation de t_str str qui sera la futur char *cmd resortie
*/
static char	*init_str_cmd(t_sh *sh, int fd_tty)
{
  char		c[4];
  int		ret;
  t_str		str;
  char		*cmd;

  t_str_constructor(&str);
  if (!(t_str_append_char(&str, '\0')))
    return (NULL);
  str.current = str.tail;
  c[0] = 0;
  while (c[0] != '\n')
    {
      memset(&c[0], 0, sizeof(char) * 4);
      if (read(0, &c[0], 4) == -1)
	return (NULL);
      if ((ret = str_handler(&str, sh, c, fd_tty)) == CTRL_D
	  && str.size == 1)
	break;
      else if (ret == FAIL)
	return (NULL);
    }
  cmd = t_str_get_str(&str);
  t_str_destructor(&str);
  return (cmd);
}

static char	*read_cmd_on_error(t_sh *sh, int fd_tty)
{
  if (fd_tty != -1)
    close(fd_tty);
  write(1, sh->prompt, strlen(sh->prompt));
  return (substitution(gnl(0), sh, 0));
}

char			*read_cmd(t_sh *sh)
{
  struct termios	t;
  int			fd_tty;
  char			*cmd;

  if ((fd_tty = open("/dev/tty", O_WRONLY)) == -1
      || !sh->env.env || !init_termcaps(&t, sh->env.env))
    return (read_cmd_on_error(sh, fd_tty));
  sh->hist.it = sh->hist.tail;
  dprintf(fd_tty, sh->prompt);
  cmd = init_str_cmd(sh, fd_tty);
  arrow_up_down(NULL, NULL, FREE_SAVE_STR, 0);
  close(fd_tty);
  cano_mod(&t);
  cmd = substitution(cmd, sh, '!');
  if (cmd && !history(&sh->hist, strdup(cmd)))
    return (NULL);
  cmd = substitution(cmd, sh, 0);
  return (cmd);
}
