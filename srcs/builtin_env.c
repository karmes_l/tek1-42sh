/*
** builtin_env.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 10:24:46 2015 VIctor Le
** Last update Sat May 23 11:51:41 2015 VIctor Le
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "builtins.h"
#include "t_cmd.h"
#include "t_sh.h"
#include "str_to_wdtab.h"

char	**version(char **env, char *name)
{
  printf("env 1.0\nCopyright (C) 2015");
  printf("42sh Project Epitech, programmation systeme unix\n");
  printf("This is free software: you are free to change and");
  printf("redistribute it.\n");
  printf("There is NO WARRANTY, to the extent permitted by law.\n\n");
  printf("Written by Metz_a, Le_l, Karmes_l, Panya_s and Lecavo_e.\n");
  return (env);
  (void)name;
}

char	**help(char **env, char *name)
{
  printf("Usage: env [OPTION]... [-] [NAME=VALUE]... [COMMAND [ARG]...]\n");
  printf("[-, -u, --unset name : Delete the variable name of \n");
  printf("the environment\n[-i, - : Begin whit an environment null] \n");
  printf("[--version : print the information on the version]\n");
  return (env);
  (void)name;
}

char	**env_ignore(char **env, char *name)
{
  free_wdtab(env);
  return (NULL);
  (void)name;
}

char		**env_option(t_cmd *cmd, char **env)
{
  int		j;
  t_token	*tmp;
  char		*name;
  char		*tab[9];
  char		**(*fptr[8])(char **env, char *name);

  j = 0;
  init_tab(tab, fptr);
  tmp = cmd->head->next;
  if (strcmp(tmp->token, "-u") == 0)
    name = tmp->next->token;
  else
    name = tmp->token;
  while (tab[j])
    {
      if (strcmp(tmp->token, tab[j]) == 0)
	env = fptr[j](env, name);
      ++j;
    }
  return (env);
}

int		builtin_env(t_cmd *cmd, t_sh *sh)
{
  char		**env;

  if (sh->env.env == NULL)
    return (0);
  env = NULL;
  env = cpy_env(env, sh);
  if (cmd->head->next != NULL)
    {
      if ((strcmp(cmd->head->next->token, "-u") == 0) && (cmd->list_size < 3))
	return (env_err(env, "option requires an argument -- 'u'"));
      if ((strcmp(cmd->head->next->token, "-0") == 0) && (cmd->list_size != 2))
	return (env_err(env, "cannot specify --null (-0) with command"));
      if ((env = env_option(cmd, env)) == NULL)
	return (1);
    }
  else
    print_env(env);
  free_wdtab(env);
  return (0);
}
