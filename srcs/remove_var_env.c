/*
** remove_var_env.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/unsetenv
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 12:19:20 2015 VIctor Le
** Last update Sun May 24 14:46:34 2015 VIctor Le
*/

#include <string.h>
#include <stdlib.h>
#include "get_var_name.h"
#include "free_and_null.h"
#include "get_nb_var.h"
#include "search_var_env.h"

static char	**malloc_new_env(char **env, int *size)
{
  char		**new_env;

  *size = get_nb_var(env);
  if ((new_env = malloc(sizeof(*new_env) * *size)) == NULL)
    return (NULL);
  return (new_env);
}

char		**delete_var_env(char **env, char *var_name)
{
  char		**new_env;
  int		i;
  int		j;
  int		size;

  i = 0;
  j = 0;
  if (env == NULL || var_name == NULL
      || (new_env = malloc_new_env(env, &size)) == NULL)
    return (NULL);
  while (i < size && env[i] != NULL)
    {
      if (env[i] == var_name)
	free(env[i++]);
      new_env[j++] = env[i++];
    }
  if (j == size)
    --j;
  new_env[j] = NULL;
  free(env);
  return (new_env);
}

char	**remove_var_env(char **env, char *var_name)
{
  char	*tmp;

  tmp = search_var_env(env, var_name, NULL);
  if (tmp == NULL)
    return (env);
  return (delete_var_env(env, tmp));
}
