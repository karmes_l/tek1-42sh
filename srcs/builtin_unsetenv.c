/*
** unsetenv.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/unsetenv
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 12:18:40 2015 VIctor Le
** Last update Sun May 24 15:46:02 2015 VIctor Le
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "remove_var_env.h"

static int	unsetenv_error(char *msg_err)
{
  fprintf(stderr, "%s\n", msg_err);
  return (2);
}

static int	builtin_unsetenv2(t_cmd *cmd, t_sh *sh)
{
  t_token	*token;
  int		i;

  token = cmd->head->next;
  while (token != NULL)
    {
      i = 0;
      while (sh->env.env[i])
	{
	  if (strstr(sh->env.env[i], token->token))
	    {
	      sh->env.env = delete_var_env(sh->env.env, sh->env.env[i]);
	      if (sh->env.env == NULL)
		return (1);
	      i = 0;
	    }
	  ++i;
	}
      token = token->next;
    }
  return (0);
}

int		builtin_unsetenv(t_cmd *cmd, t_sh *sh)
{
  if (cmd == NULL || sh == NULL || sh->env.env == NULL)
    return (1);
  if (cmd->list_size < 2)
    return (unsetenv_error("unsetenv: Too few arguments."));
  return (builtin_unsetenv2(cmd, sh));
}
