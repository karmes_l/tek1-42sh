/*
** is_correct_quoted.c for 42sh in /home/le_l/workspace/42sh
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 22 14:13:29 2015 VIctor Le
** Last update Sun May 24 15:15:30 2015 VIctor Le
*/

#include <stdio.h>

int	is_correct_quoted(char *s)
{
  int	i;
  char	quote;
  char	open;

  open = 0;
  quote = '\0';
  i = 0;
  while (s[i] != '\0')
    {
      if (!quote && (s[i] == '"' || s[i] == '\''))
	quote = s[i];
      if (quote && quote == s[i])
	{
	  open = (!open) ? 1 : 0;
	  if (open == 0)
	    quote = '\0';
	}
      ++i;
    }
  if (open)
    {
      fprintf(stderr, "Unmatch %c.\n", quote);
      return (0);
    }
  return (1);
}
