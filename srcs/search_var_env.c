/*
** search_var_env.c for 42sh in /home/le_l/workspace/42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 19 22:59:15 2015 VIctor Le
** Last update Tue May 19 23:29:03 2015 VIctor Le
*/

#include <stdlib.h>
#include <string.h>

int	strlen_var_name(char *env_line)
{
  int	i;

  if (env_line == NULL)
    return (-1);
  i = 0;
  while (env_line[i] != '\0' && env_line[i] != '=')
    ++i;
  return (i);
}

char	*search_var_env(char **env, char *var_name, int *line)
{
  int	i;

  if (env == NULL || var_name == NULL)
    return (NULL);
  i = 0;
  while (env[i] != NULL && strncmp(var_name, env[i], strlen_var_name(env[i])))
    ++i;
  if (line != NULL)
    *line = i;
  return (env[i]);
}
