/*
** epure_str.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Mon May 18 16:02:45 2015 lionel karmes
** Last update Sun May 24 15:16:57 2015 VIctor Le
*/

#include <stdlib.h>

void	decal_str(char *str, int i)
{
  while (str[i] != '\0')
    {
      str[i] = str[i + 1];
      i++;
    }
}

int	space_or_tab(char c)
{
  if (c == ' '  || c == '\t')
    return (1);
  return (0);
}

void	epur_str(char *str)
{
  int	i;

  i = 0;
  if (str != NULL)
    {
      while (str[i] != '\0')
	{
	  if (i == 0 && space_or_tab(str[i]))
	    decal_str(str, i);
	  else if (space_or_tab(str[i]) &&
		   (space_or_tab(str[i + 1]) || str[i + 1] == '\0'))
	    decal_str(str, i);
	  else
	    i++;
	}
    }
}

void	delete_backspace(char *str)
{
  int	i;

  if (str)
    {
      i = 0;
      while (str[i])
	{
	  if (str[i] == '\n')
	    decal_str(str, i);
	  ++i;
	}
    }
}
