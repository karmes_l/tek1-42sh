/*
** t_str.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 17:49:16 2015 VIctor Le
** Last update Tue May 19 18:18:37 2015 Aurélien Metz
*/

#include <stdlib.h>
#include "t_str.h"
#include "t_char.h"

void	t_str_constructor(t_str *str)
{
  str->head = NULL;
  str->tail = NULL;
  str->current = NULL;
  str->size = 0;
}

int		t_str_append_char(t_str *str, char c)
{
  t_char	*node;

  if ((node = malloc(sizeof(*node))) == NULL)
    return (0);
  t_char_constructor(node);
  node->c = c;
  node->prev = str->tail;
  if (str->head == NULL)
    str->head = node;
  if (str->tail != NULL)
    str->tail->next = node;
  str->tail = node;
  ++str->size;
  return (1);
}

int		t_str_cpy(t_str *str, const char *s)
{
  unsigned int	i;

  i = 0;
  while (s[i])
    if (t_str_append_char(str, s[i++]) == 0)
      return (0);
  return (1);
}

char		*t_str_get_str(t_str *str)
{
  t_char	*it;
  char		*ret;
  int		i;

  if (str->size == 0 || (ret = malloc(sizeof(*ret) * (str->size + 1))) == NULL)
    return (NULL);
  i = 0;
  it = str->head;
  while (it != NULL && i < str->size + 1)
    {
      ret[i] = it->c;
      it = it->next;
      ++i;
    }
  ret[i] = '\0';
  return (ret);
}

void		t_str_destructor(t_str *str)
{
  t_char	*tmp;

  if (str != NULL)
    {
      tmp = str->head;
      while (tmp != NULL && str->size != 0)
	{
	  str->head = str->head->next;
	  t_char_destructor(tmp);
	  --str->size;
	  tmp = str->head;
	}
    }
}
