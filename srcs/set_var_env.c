/*
** set_var_env.c for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 14 18:40:20 2015 VIctor Le
** Last update Sat May 23 11:53:38 2015 VIctor Le
*/

#include <stdlib.h>
#include <string.h>
#include "create_var_env.h"
#include "get_var_name.h"
#include "search_var_env.h"

static char	**give_new_value(char **env, int i, char *tmp, char *value)
{
  if ((env[i] = malloc(sizeof(**env)
		       * (strlen_var_name(tmp) + strlen(value) + 2))) == NULL)
    return (0);
  env[i][0] = '\0';
  strncat(env[i], tmp, strlen_var_name(tmp));
  strcat(env[i], "=");
  strcat(env[i], value);
  free(tmp);
  return (env);
}

char	**set_var_env(char **env, char *var_name, char *value)
{
  char	*tmp;
  int	i;

  if (var_name == NULL || value == NULL)
    return (env);
  tmp = search_var_env(env, var_name, &i);
  if (tmp == NULL)
    return (create_var_env(env, var_name, value));
  return (give_new_value(env, i, tmp, value));
}
