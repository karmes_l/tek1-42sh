/*
** gnl.c for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May  9 15:38:35 2015 VIctor Le
** Last update Thu May 21 15:44:16 2015 VIctor Le
*/

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "gnl.h"

static char	*gnl_strncat(char *dest, const char *src,
			     const int dest_size, const int end)
{
  int		i;
  int		j;

  if (dest == NULL || src == NULL)
    return (NULL);
  i = dest_size;
  j = 0;
  while (i < end)
    dest[i++] = src[j++];
  return (dest);
}

static char	*gnl_realloc(const char *buffer, char *str,
			     int *size, const int add)
{
  char		*new_str;
  int		i;

  if (*size + add + 1 <= 0 ||
      (new_str = malloc(sizeof(*new_str) * (*size + add + 1))) == NULL)
    return (NULL);
  i = 0;
  while (i <= *size + add)
    new_str[i++] = 0;
  if (str != NULL)
    {
      i = -1;
      while (++i < *size)
	new_str[i] = str[i];
      free(str);
    }
  gnl_strncat(new_str, buffer, *size, *size + add);
  *size += add;
  return (new_str);
}

static int	gnl_copy(const int char_read, const char *buffer,
			 char **line, int *i)
{
  static int	size;
  int		add;

  add = 0;
  while (add < char_read - *i && buffer[*i + add] != '\n')
    add += 1;
  add = (buffer[*i + add] == '\n') ? add + 1 : add;
  *line = gnl_realloc(buffer + *i, *line, &size, add);
  if (buffer[*i + add - 1] == '\n')
    {
      *i = (*i + add >= char_read) ? 0 : *i + add;
      size = 0;
      return (1);
    }
  *i = (*i + add >= char_read) ? 0 : *i + add;
  return (0);
}

char		*gnl(const int fd)
{
  static char	buffer[GNL_READSIZE + 1];
  char		*line;
  static int	char_read;
  static int	i;

  line = NULL;
  if (i > 0 && gnl_copy(char_read, buffer, &line, &i))
    return (line);
  while (i == 0)
    {
      if ((char_read = read(fd, buffer, GNL_READSIZE)) == -1)
	return (NULL);
      else if (char_read == 0 && line == NULL)
	return (strdup("\0"));
      else if (char_read == 0 || gnl_copy(char_read, buffer, &line, &i))
	return (line);
    }
  return (line);
}
