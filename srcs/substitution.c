/*
** substitution.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh
** 
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
** 
** Started on  Tue May 19 17:45:03 2015 Aurélien Metz
** Last update Sun May 24 12:42:22 2015 Aurélien Metz
*/

#include "substitution.h"

static	char	*sub_free(t_str *str, char **wdtab, char *s)
{
  t_str_destructor(str);
  free_wdtab(wdtab);
  return (s);
}

static int	glob_sub(t_str *str, glob_t *gl)
{
  unsigned int	i;

  i = 0;
  while (i < gl->gl_pathc)
    if (t_str_cpy(str, gl->gl_pathv[i++]) == 0
	|| t_str_append_char(str, ' ') == 0)
      {
	globfree(gl);
	return (0);
      }
  globfree(gl);
  return (1);
}

static int	substitue(const t_sh *sh, t_str *str,
			  const char *s, const char fmt)
{
  glob_t	gl;
  char		*hist;
  char		*sub;
  char		ret;

  if (s == NULL)
    return (1);
  if (s[0] == '$' && !fmt && (sub = find_var_env(sh->env.env, s + 1)))
    return (t_str_cpy(str, sub));
  else if (s[0] == '!' && fmt && str_in_str(s + 1, NUM))
    {
      hist = find_hist(&sh->hist, get_nbr(s + 1));
      if (hist == NULL || strcmp(hist, s) == 0)
	return (t_str_cpy(str, s));
      sub = substitution(strdup(hist), sh, fmt);
      ret = t_str_cpy(str, sub);
      free(sub);
      return (ret);
    }
  else if (!fmt && (char_in_str('*', s)
		    || char_in_str('?', s) || char_in_str('[', s)))
    if (glob(s, 0, NULL, &gl) == 0)
      return (glob_sub(str, &gl));
  return (t_str_cpy(str, s));
}

char		*substitution(char *s, const t_sh *sh, const char fmt)
{
  t_str		new;
  char		*ret;
  char		**wdtab;
  unsigned int	i[2];

  t_str_constructor(&new);
  if ((wdtab = str_to_wdtab(s, META_CHARS)) == NULL)
    return (s);
  i[0] = 0;
  i[1] = 0;
  while (s[i[0]])
    {
      while (char_in_str(s[i[0]], META_CHARS))
	if ((t_str_append_char(&new, s[i[0]++]) == 0))
	  return (sub_free(&new, wdtab, s));
      if (substitue(sh, &new, wdtab[i[1]], fmt) == 0)
	return (sub_free(&new, wdtab, s));
      if (wdtab[i[1]])
	i[0] += strlen(wdtab[i[1]++]);
    }
  if ((ret = t_str_get_str(&new)))
    free(s);
  (void)sub_free(&new, wdtab, s);
  return (ret ? ret : s);
}
