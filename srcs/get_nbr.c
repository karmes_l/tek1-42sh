/*
** get_nbr.c for 42sh in /home/le_l/rendu/Unix/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 15:52:55 2015 VIctor Le
** Last update Sat May 23 17:49:02 2015 VIctor Le
*/

static int	has_overflow(const char sign, const int value,
			     const int old_value)
{
  if ((sign > 0 && old_value > value)
      || (sign < 0 && sign * value > sign * old_value))
    return (1);
  return (0);
}

static char	get_sign(const char **nb)
{
  char		sign;

  sign = 1;
  while (**nb == '-' || **nb == '+')
    {
      sign = (**nb == '-') ? -sign : sign;
      ++(*nb);
    }
  return (sign);
}

int	is_nbr(const char *nb)
{
  while (*nb != '\0')
    {
      if ((*nb < '0' || *nb > '9') && *nb != '-' && *nb != '+')
	return (0);
      ++nb;
    }
  return (1);
}

int	get_nbr(const char *nb)
{
  int	value;
  int	old_value;
  char	sign;

  if (!nb)
    return (0);
  while (*nb == ' ' || *nb == '\t')
    ++nb;
  if (!is_nbr(nb))
    return (0);
  sign = get_sign(&nb);
  value = 0;
  old_value = 0;
  while (*nb != '\0' && !has_overflow(sign, value, old_value))
    {
      old_value = value;
      value = value * 10 + *nb - '0';
      ++nb;
    }
  if (has_overflow(sign, value, old_value))
    return (0);
  return (sign * value);
}
