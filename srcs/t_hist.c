/*
** t_hist.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May 13 10:40:36 2015 lionel karmes
** Last update Fri May 15 15:05:37 2015 lionel karmes
*/

#include <stdlib.h>
#include "t_hist.h"
#include "free_and_null.h"

void	t_hist_constructor(t_hist *hist)
{
  hist->next = NULL;
  hist->prev = NULL;
  hist->cmd = NULL;
}

t_hist		*t_hist_destructor(t_hist *hist)
{
  t_hist	*ret;

  ret = NULL;
  if (hist != NULL)
    {
      ret = hist->next;
      free_and_null((void **)&hist->cmd);
      if (hist->next != NULL)
	hist->next->prev = hist->prev;
      if (hist->prev != NULL)
	hist->prev->next = hist->next;
      free_and_null((void **)&hist);
    }
  return (ret);
}
