/*
** exec_simple_cmd.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:38:13 2015 VIctor Le
** Last update Sun Jun  7 22:37:32 2015 VIctor Le
*/

#include <string.h>
#include <stdio.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "exec_cmd.h"
#include "exec_builtin.h"
#include "exec_shell_cmd.h"
#include "search_cmd.h"
#include "builtins.h"

static void	fill_builtin_name(char **builtin_name)
{
  builtin_name[0] = BUILTIN_CD;
  builtin_name[1] = BUILTIN_ECHO;
  builtin_name[2] = BUILTIN_ENV;
  builtin_name[3] = BUILTIN_SETENV;
  builtin_name[4] = BUILTIN_UNSETENV;
  builtin_name[5] = BUILTIN_EXIT;
  builtin_name[6] = BUILTIN_HISTORY;
  builtin_name[7] = NULL;
}

/*
** Détermine si la cmd donnée est un builtin.
** Retourne un index pour un tableau de fonctions si s'en est un, -1 sinon.
*/
static int	is_builtin(t_cmd *cmd)
{
  char		*builtin_name[NB_BUILTIN + 1];
  int		i;

  if (cmd->head == NULL || cmd->head->token == NULL)
    return (-1);
  fill_builtin_name(builtin_name);
  i = 0;
  while (builtin_name[i] != NULL && strcmp(cmd->head->token, builtin_name[i]))
    ++i;
  if (builtin_name[i] == NULL)
    return (-1);
  return (i);
}

static char	*get_cmd_token(t_cmd *cmd)
{
  t_token	*token;
  char		*redir[5];
  int		i;

  redir[0] = ">";
  redir[1] = ">>";
  redir[2] = "<";
  redir[3] = "<<";
  redir[4] = NULL;
  token = cmd->head;
  while (token)
    {
      i = 0;
      while (redir[i] != NULL && strcmp(token->token, redir[i]))
	++i;
      if (redir[i] == NULL)
	return (token->token);
      token = token->next->next;
    }
  return (NULL);
}

int	exec_simple_cmd(t_cmd *cmd, t_sh *sh, int forked)
{
  int	builtin_ret;
  char	*tmp;

  if ((builtin_ret = is_builtin(cmd)) != -1)
    return (exec_builtin(cmd, sh, builtin_ret));
  if ((tmp = get_cmd_token(cmd)) == NULL)
    {
      fprintf(stderr, "Internal error\n");
      return (EXEC_ERROR);
    }
  if ((tmp = search_cmd(sh->env.path, tmp)) != NULL)
    return (exec_shell_cmd(cmd, sh, tmp, forked));
  else
    sh->cmd_return = 1;
  return (EXEC_OK);
}
