/*
** t_file.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May  8 12:21:18 2015 lionel karmes
** Last update Sat May 16 07:49:00 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_file.h"
#include "free_and_null.h"

t_file		*t_file_constructor(char *path, int flags)
{
  t_file	*node;

  if (!path || !(node = malloc(sizeof(t_file))))
    return (NULL);
  node->path = path;
  node->flags = flags;
  return (node);
}

void	t_file_destructor(t_file *file)
{
  if (file)
    {
      free_and_null((void **)&file->path);
      free_and_null((void **)&file);
    }
}
