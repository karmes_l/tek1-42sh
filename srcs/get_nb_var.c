/*
** get_nb_var.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 10:36:32 2015 VIctor Le
** Last update Sat May 23 11:58:05 2015 VIctor Le
*/

int	get_nb_var(char **env)
{
  int	i;

  if (!env)
    return (0);
  i = 0;
  while (env[i])
    ++i;
  return (i);
}
