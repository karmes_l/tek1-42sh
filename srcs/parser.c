/*
** rework.c for 42sh in /home/le_l/workspace/42sh/decoupage/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May  8 07:45:36 2015 VIctor Le
** Last update Sun May 24 19:15:07 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_list.h"
#include "t_str.h"
#include "metachar_handler.h"
#include "quote_handler.h"

static int	parser_loop(char *s, int *i, t_str *str, t_list *list)
{
  char		meta_ret;
  char		quote_ret;

  if ((meta_ret = metachar_handler(s, i, str, list)) == META_FAIL)
    return (0);
  else if (meta_ret == NOT_META)
    {
      if ((quote_ret = quote_handler(&s, i, str)) == QUOTE_FAIL)
	return (0);
      else if (quote_ret == NOT_QUOTE && !t_str_append_char(str, s[*i]))
	return (0);
      if (s[*i] != '\0')
	++(*i);
    }
  return (1);
}

int	parser(char *s, t_list *list)
{
  t_str	str;
  int	i;
  int	ok;

  if (s == NULL || list == NULL || !t_list_append_empty_cmd(list))
    return (0);
  t_str_constructor(&str);
  i = 0;
  ok = 1;
  while (ok && s[i] != '\0')
    ok = parser_loop(s, &i, &str, list);
  if (str.size != 0)
    push_back_token(&str, list->tail, 1);
  t_str_destructor(&str);
  return (ok);
}
