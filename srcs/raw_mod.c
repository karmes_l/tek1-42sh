/*
** raw_mod.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Thu May 14 12:51:26 2015 lionel karmes
** Last update Sun May 24 16:14:06 2015 lionel karmes
*/

#include <string.h>
#include <stdlib.h>
#include "termcaps.h"

int	modify_terminal(struct termios *t)
{
  if (tcsetattr(0, TCSANOW, t) == -1)
    {
      fprintf(stderr, "%s\n", "Error tcsetattr");
      return (FAIL);
    }
  return (TERM_OK);
}

int	cano_mod(struct termios *t)
{
  t->c_lflag |= ICANON;
  t->c_lflag |= ECHO;
  t->c_cc[VMIN] = 1;
  t->c_cc[VTIME] = 0;
  return (modify_terminal(t));
}

int	raw_mod(struct termios *t)
{
  t->c_lflag &= ~ICANON;
  t->c_lflag &= ~ECHO;
  return (modify_terminal(t));
}

char	*get_term_env(char **env)
{
  int	i;

  i = 0;
  if (env)
    while (env[i] != NULL)
      {
	if (!strncmp(env[i], "TERM=", 5))
	  return (env[i] + 5);
	++i;
      }
  return ("xterm");
}

int	init_termcaps(struct termios *t, char **env)
{
  if (tcgetattr(0, t) == -1)
    {
      fprintf(stderr, "%s\n", "Error tcgetattr");
      return (FAIL);
    }
  if (raw_mod(t) != TERM_OK)
    return (FAIL);
  if (tgetent(NULL, get_term_env(env)) != 1)
    return (FAIL);
  return (TERM_OK);
}
