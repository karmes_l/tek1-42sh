/*
** exec_pipe.c for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:36:17 2015 VIctor Le
** Last update Sat Jun  6 11:19:45 2015 VIctor Le
*/

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include "t_cmd.h"
#include "t_sh.h"
#include "exec_cmd.h"
#include "exec_simple_cmd.h"
#include "redirection.h"
#include "read_signal.h"

/*
** Execution d'une commande pipe dans le fils.
** Retourne dans TOUS les cas EXEC_PIPE.
*/
static int	exec_pipe_in_child(t_cmd *cmd, t_sh *sh,
				   int *pipefd, int *save_fd)
{
  if (dup2(*save_fd, 0) == -1)
    {
      fprintf(stderr, "%s\n", strerror(errno));
      return (EXEC_PIPE);
    }
  if (cmd->operator[1] == OP_PIPE)
    {
      if (dup2(pipefd[1], 1) == -1)
	{
	  fprintf(stderr, "%s\n", strerror(errno));
	  return (EXEC_PIPE);
	}
    }
  close(pipefd[0]);
  (void)exec_simple_cmd(cmd, sh, 1);
  return (EXEC_PIPE);
}

static void	exec_father(int *pipefd, t_cmd **cmd, t_sh *sh, pid_t pid)
{
  close(pipefd[1]);
  if ((*cmd)->next == NULL || (*cmd)->next->operator[0] != OP_PIPE)
    waitpid(pid, &(*cmd)->ret, 0);
  if (WIFSIGNALED((*cmd)->ret) && WTERMSIG((*cmd)->ret) == 11)
    printf("Segmentation fault\n");
  sh->cmd_return = (*cmd)->ret;
  *cmd = (*cmd)->next;
}

static int	exec_pipe_loop(t_cmd **cmd, t_sh *sh, int *save_fd)
{
  int		pipefd[2];
  pid_t		pid;

  if (pipe(pipefd) == -1 || (pid = fork()) == -1)
    {
      fprintf(stderr, "%s\n", strerror(errno));
      return (EXEC_ERROR);
    }
  if (!pid)
    return (exec_pipe_in_child(*cmd, sh, pipefd, save_fd));
  if (*save_fd > 0)
    close(*save_fd);
  exec_father(pipefd, cmd, sh, pid);
  *save_fd = pipefd[0];
  return (EXEC_OK);
}

int		exec_pipe(t_cmd *cmd, t_sh *sh)
{
  int		save_fd;
  int		ret;

  save_fd = 0;
  read_signal(PROC_SON);
  if ((ret = exec_pipe_loop(&cmd, sh, &save_fd)) == EXEC_ERROR
      || ret == EXEC_PIPE)
    return (ret);
  while (cmd != NULL && cmd->operator[0] == OP_PIPE)
    {
      if ((ret = exec_pipe_loop(&cmd, sh, &save_fd)) == EXEC_ERROR
	  || ret == EXEC_PIPE)
	return (ret);
    }
  return (EXEC_OK);
}
