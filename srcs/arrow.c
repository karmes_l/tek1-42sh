/*
** arrow.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May 15 17:09:07 2015 lionel karmes
** Last update Mon May 18 16:15:12 2015 lionel karmes
*/

#include <stdlib.h>
#include "termcaps.h"
#include "t_str.h"

/*
** Deplacement du curseur à gauche (K_LEFT) ou à droite (K_RIGHT) (si possible)
*/
static void	arrow_left_right(t_str *str, int arrow, int fd)
{
  char		*cap;

  if (arrow == K_LEFT && str->current->prev != NULL)
    {
      str->current = str->current->prev;
      cap = tgetstr("le", NULL);
      dprintf(fd, "%s", cap);
    }
  else if (arrow == K_RIGHT && str->current->next != NULL)
    {
      str->current = str->current->next;
      cap = tgetstr("nd", NULL);
      dprintf(fd, "%s", cap);
    }
}

/*
** Si on n'est pas encore remonté dans l'historique : load t_str *save_str
** Remplace t_str *str par t_str *hist
** Modifie t_hist **hist si t_str *save_str exist (et si possible)
** Place le curseur à la fin de t_str *str
*/
static int	arrow_up(t_str *str, t_hist **hist, t_str *save_str)
{
  int		saving_str;

  saving_str = 0;
  if ((*hist)->next == NULL && save_str->size == 0)
    {
      if (!t_str_dup(save_str, str))
	return (FAIL);
      saving_str = 1;
    }
  t_str_destructor(str);
  if (!saving_str && (*hist)->prev)
    *hist = (*hist)->prev;
  if (!char_to_str(str, (*hist)->cmd))
    return (FAIL);
  str->current = str->tail;
  return (TERM_OK);
}

/*
** Si on n'est pas dans l'historique ne fais rien
** Modife t_hist **hist (si possible)
** Si on n'est pas dans l'historique et que save_str existe et != str :
**	remplace t_str *str par t_str *hist
** Sinon : remplace t_str *str par t_str *str_save
*/
static int		arrow_down(t_str *str, t_hist **hist, t_str *save_str)
{
  if (save_str->size == 0 || str == save_str)
    return (TERM_OK);
  t_str_destructor(str);
  if ((*hist)->next)
    {
      *hist = (*hist)->next;
      if (!char_to_str(str, (*hist)->cmd))
	return (FAIL);
      str->current = str->tail;
    }
  else
    {
      if (!t_str_dup(str, save_str))
	return (FAIL);
      str->current = str->tail;
      t_str_destructor(save_str);
    }
  return (TERM_OK);
}

int		arrow_up_down(t_str *str, t_hist **hist, int arrow, int fd)
{
  static t_str	save_str = {NULL, NULL, NULL, 0};
  int		ret;
  char		*cap;

  if (arrow == FREE_SAVE_STR)
    {
      t_str_destructor(&save_str);
      return (TERM_OK);
    }
  ret = 2;
  if (!(*hist))
    return (ret);
  if (!(arrow == K_DOWN && (save_str.size == 0 || str == &save_str)))
    {
      pos_cursor_current(fd, str, str->head);
      cap = tgetstr("ce", NULL);
      dprintf(fd, "%s", cap);
      ret = TERM_OK;
    }
  if (arrow == K_UP && !arrow_up(str, hist, &save_str))
    return (FAIL);
  if (arrow == K_DOWN && !arrow_down(str, hist, &save_str))
    return (FAIL);
  return (ret);
}

int		arrow_handler(t_str *str, t_hist **hist, int arrow, int fd)
{
  int		ret;

  if (arrow == K_LEFT || arrow == K_RIGHT)
    arrow_left_right(str, arrow, fd);
  else
    {
      if ((ret = arrow_up_down(str, hist, arrow, fd)) == FAIL)
	return (FAIL);
      else if (ret == 2)
	return (TERM_OK);
      aff_str(fd, str);
    }
  return (TERM_OK);
}
