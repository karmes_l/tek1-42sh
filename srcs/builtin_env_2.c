/*
** builting_env_2.c for builting env 2 in /home/lecavo_e/PROJET/42sh/PSU_2014_42sh/srcs
**
** Made by enora lecavorzin-metallinos
** Login   <lecavo_e@epitech.net>
**
** Started on  Tue May 19 16:14:27 2015 enora lecavorzin-metallinos
** Last update Sun May 24 15:58:04 2015 VIctor Le
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "builtins.h"
#include "t_cmd.h"
#include "t_sh.h"
#include "remove_var_env.h"

char		**env_zero(char **env, char *name)
{
  unsigned int	i;

  i = 0;
  while (env[i])
    {
      my_putstr(env[i]);
      i++;
    }
  return (env);
  (void)name;
}

char		**env_unset(char **env, char *name)
{
  if (name == NULL)
    return (env);
  env = remove_var_env(env, name);
  print_env(env);
  return (env);
}

char		**print_env(char **env)
{
  unsigned int	i;

  if (env == NULL)
    return (env);
  i = 0;
  while (env[i])
    {
      my_putstr(env[i]);
      my_putchar('\n');
      i++;
    }
  return (env);
}

void		init_tab(char **tab,
			   char **(*fptr[9])(char**, char *))
{
  tab[0] = "--help";
  tab[1] = "--version";
  tab[2] = "-unset";
  tab[3] = "-u";
  tab[4] = "-";
  tab[5] = "-i";
  tab[6] = "--ignore_environment";
  tab[7] = "-0";
  tab[8] = NULL;
  fptr[0] = &help;
  fptr[1] = &version;
  fptr[2] = &env_unset;
  fptr[3] = &env_unset;
  fptr[4] = &env_ignore;
  fptr[5] = &env_ignore;
  fptr[6] = &env_ignore;
  fptr[7] = &env_zero;
}

char		**cpy_env(char **env, t_sh *sh)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  while (sh->env.env[i] != NULL)
    ++i;
  if ((env = malloc(sizeof(char *) * (i + 1))) == NULL)
    return (NULL);
  while (j < i)
    {
      if ((env[j] = strdup(sh->env.env[j])) == NULL)
	return (NULL);
      ++j;
    }
  env[j] = NULL;
  return (env);
}
