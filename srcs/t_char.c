/*
** t_char.c for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 17:53:47 2015 VIctor Le
** Last update Mon May 18 12:51:55 2015 VIctor Le
*/

#include <stdlib.h>
#include "t_char.h"
#include "free_and_null.h"

void	t_char_constructor(t_char *c)
{
  c->next = NULL;
  c->prev = NULL;
  c->c = 0;
}

void	t_char_destructor(t_char *c)
{
  if (c != NULL)
    {
      if (c->next)
	c->next->prev = c->prev;
      if (c->prev)
	c->prev->next = c->next;
      free_and_null((void **)&c);
    }
}
