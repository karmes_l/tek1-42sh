/*
** read_signal.c for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/signal
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Tue May 12 17:31:08 2015 lionel karmes
** Last update Fri Jun  5 14:40:31 2015 VIctor Le
*/

#include <stdio.h>
#include "read_signal.h"

void	read_signal(int procc)
{
  if (procc == PROC_DAD)
    {
      if (signal(SIGINT, get_sigint_dad) == SIG_ERR)
	fprintf(stderr, ("[ERROR] : SIGINT\n"));
      if (signal(SIGQUIT, get_sigquit_dad) == SIG_ERR)
	fprintf(stderr, ("[ERROR] : SIGQUIT\n"));
    }
  else
    {
      if (signal(SIGINT, get_sigint_son) == SIG_ERR)
	fprintf(stderr, ("[ERROR] : SIGINT\n"));
      if (signal(SIGQUIT, get_sigquit_son) == SIG_ERR)
	fprintf(stderr, ("[ERROR] : SIGQUIT\n"));
    }
  if (signal(SIGTSTP, SIG_IGN) == SIG_ERR)
    fprintf(stderr, ("[ERROR] : SIGTSTP\n"));
  if (signal(SIGCHLD, get_sigchld) == SIG_ERR)
    fprintf(stderr, ("[ERROR] : SIGCHLD\n"));
}
