/*
** operator_handler.c for 42sh in /home/metz_a
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sat May  9 12:18:11 2015 Aurélien Metz
** Last update Sun May 24 16:25:46 2015 lionel karmes
*/

#include <string.h>
#include "t_list.h"
#include "t_op.h"

void	t_op_constructor(t_op op_handler[NBR_OP])
{
  op_handler[0].op_value = OP_PIPE;
  strcpy(op_handler[0].op_str, PIPE);
  op_handler[1].op_value = OP_OR;
  strcpy(op_handler[1].op_str, OR);
  op_handler[2].op_value = OP_BG;
  strcpy(op_handler[2].op_str, BG);
  op_handler[3].op_value = OP_AND;
  strcpy(op_handler[3].op_str, AND);
  op_handler[4].op_value = OP_NEWLINE;
  strcpy(op_handler[4].op_str, NEWLINE);
  op_handler[5].op_value = OP_SEMICOLON;
  strcpy(op_handler[5].op_str, SEMICOLON);
}

int		operator_handler(t_list *list)
{
  static t_op	op_handler[NBR_OP];
  static char	first_call = 1;
  int		i;

  if (first_call)
    t_op_constructor(op_handler);
  i = 0;
  first_call = 0;
  while (i < NBR_OP)
    {
      if (strcmp(list->tail->tail->token, op_handler[i].op_str) == 0)
	{
	  t_cmd_remove_token(list->tail, list->tail->tail);
	  list->tail->operator[1] = op_handler[i].op_value;
	  if (op_handler[i].op_value != OP_NEWLINE)
	    {
	      if (t_list_append_empty_cmd(list) == 0)
		return (0);
	      list->tail->operator[0] = op_handler[i].op_value;
	    }
	  return (1);
	}
      i = i + 1;
    }
  return (1);
}
