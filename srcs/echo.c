/*
** echo.c for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh/workspace/parser
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sat May 16 13:49:38 2015 Aurélien Metz
** Last update Fri Jun  5 17:52:17 2015 VIctor Le
*/

#include <string.h>
#include <unistd.h>
#include "t_list.h"
#include "t_sh.h"
#include "echo.h"

void		echo(t_token *token)
{
  char		n;
  unsigned int	i;

  n = 1;
  while (token && !strcmp(token->token, "-n"))
    {
      n = 0;
      token = token->next;
    }
  while (token)
    {
      i = 0;
      while (token->token[i])
	{
	  (void)write(1, token->token + i, 1);
	  ++i;
	}
      token = token->next;
      if (token)
	(void)write(1, " ", 1);
    }
  if (n)
    (void)write(1, "\n", 1);
}
