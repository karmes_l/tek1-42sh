##
## Makefile for corewar in /home/metz_a/rendu/Prog elem/CPE_2014_corewar
##
## Made by Aurélien Metz
## Login   <metz_a@epitech.net>
##
## Started on  Thu Mar  5 11:41:05 2015 Aurélien Metz
## Last update Sun May 24 22:13:49 2015 VIctor Le
##

#####################################################################
all:
	@printf "\033[032m --- [SOURCES] ---\033[0m\n"
	@cd srcs && make -f Makefile

clean:
	@printf "\033[031m --- [CLEAN] ---\033[0m\n"
	@cd srcs && make clean -f Makefile

fclean:	clean
	@printf "\033[031m --- [FCLEAN] ---\033[0m\n"
	@cd srcs && make fclean -f Makefile

re:	fclean all

.PHONY:	all clean fclean re
#####################################################################
