/*
** remove_var_env.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 09:32:12 2015 VIctor Le
** Last update Sun May 24 14:20:08 2015 VIctor Le
*/

#ifndef REMOVE_VAR_ENV_H_
# define REMOVE_VAR_ENV_H_

/*
** Retire de l'environnement donné, la variable désignée.
*/
char	**delete_var_env(char **env, char *var_name);

/*
** @env:	l'environnement à modifier
** @var_name:	la variable à retirer.
** Retourne l'env modifié, NULL sur erreur (attention perte de l'env possible).
*/
char	**remove_var_env(char **env, char *var_name);

#endif /* !REMOVE_VAR_ENV_H_ */
