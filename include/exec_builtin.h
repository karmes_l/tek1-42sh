/*
** exec_builtin.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:41:33 2015 VIctor Le
** Last update Mon May 18 09:45:37 2015 VIctor Le
*/

#ifndef EXEC_BUILTIN_H_
# define EXEC_BUILTIN_H_

# include "t_cmd.h"
# include "t_sh.h"

/*
** Exécute un builtin
** Retourne EXEC_OK.
** @i:	L'index du builtin à appeler. (cf. exec_cmd.h pour les index)
*/
int	exec_builtin(t_cmd *cmd, t_sh *sh, int i);

#endif /* !EXEC_BUILTIN_H_ */
