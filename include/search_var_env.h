/*
** search_var_env.h for 42sh in /home/le_l/workspace/42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May 19 23:17:24 2015 VIctor Le
** Last update Tue May 19 23:28:51 2015 VIctor Le
*/

#ifndef SEARCH_VAR_ENV_H_
# define SEARCH_VAR_ENV_H_

int	strlen_var_name(char *env_line);

/*
** Recherche une variable d'env par son nom.
** Retourne la ligne correspondante.
** @line:       Y sera stocke le numéro de ligne dans l'env.
*/
char	*search_var_env(char **env, char *var_name, int *line);

#endif /* !SEARCH_VAR_ENV_H_ */
