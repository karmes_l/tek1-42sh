/*
** t_token.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:25:25 2015 VIctor Le
** Last update Sat May  9 16:11:26 2015 VIctor Le
*/

#ifndef T_TOKEN_H_
# define T_TOKEN_H_

/*
** Structure contenant un token d'une cmd.
** @next:	Token suivant de la cmd.
** @prev:	Token précédent de la cmd.
** @token:	Le token de la cmd.
** @isword:	Indique si c'est un mot ou un mot spécial. Booléen.
**		Permet de faire des trucs du genre 'touch "<<"'
**		Usage pour implémentation des quotes.
*/
typedef struct		s_token
{
  struct s_token	*next;
  struct s_token	*prev;
  char			*token;
  char			isword;
}			t_token;

void	t_token_constructor(t_token *token);

/*
** Renvoie le token suivant.
*/
t_token	*t_token_destructor(t_token *token);

#endif /* !T_TOKEN_H_ */
