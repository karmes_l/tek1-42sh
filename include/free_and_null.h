/*
** free_and_null.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:40:57 2015 VIctor Le
** Last update Fri May 15 22:55:58 2015 VIctor Le
*/

#ifndef FREE_AND_NULL_H_
# define FREE_AND_NULL_H_

/*
** Usage: free_and_null((void **)&ptr);
** Vérifie, free, set à NULL le pointeur donné.
*/
void	free_and_null(void **ptr);

#endif /* !FREE_AND_NULL_H_ */
