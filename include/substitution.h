/*
** substitution.h for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Tue May 19 17:46:25 2015 Aurélien Metz
** Last update Sun May 24 15:31:59 2015 VIctor Le
*/

#ifndef SUBSTITUTION_H_
# define SUBSTITUTION_H_

# include <stdlib.h>
# include <string.h>
# include <glob.h>
# include "t_str.h"
# include "t_sh.h"
# include "history.h"
# include "get_nbr.h"
# include "str_to_wdtab.h"

# define META_CHARS		("|&;()<> \t\v\n\"")
# define NUM			("-0123456789")

char	*substitution(char *s, const t_sh *sh, const char fmt);

/*
** Retourne un pointeur sur le contenu de la variable d'environement
** dont le nom est passé en paramètre sous forme de char *.
** Si la variable n'hexiste pas retourne (NULL).
*/
char	*find_var_env(char ** const env, const char *var);

/*
** Retourne 1 si le caractère c est contenue dans s (char *).
** sinnon retourne 0.
*/
int	char_in_str(const char c, const char *s);

/*
** Retourne 1 si l'intégralitée des caractères contenue dans s (char *)
** sont également contenue dans ref (char *). sinnon retourne 0.
*/
int	str_in_str(const char *s, const char *ref);

#endif /* !SUBSTITUTION_H_ */
