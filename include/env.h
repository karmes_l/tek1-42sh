/*
** env.h for env in /home/panya_s/Unix_system_Programminig/env
**
** Made by Saysana Panya
** Login   <panya_s@epitech.net>
**
** Started on  Mon May 11 15:43:35 2015 Saysana Panya
** Last update Sat May 16 09:46:58 2015 VIctor Le
*/

#ifndef ENV_H_
# define ENV_H_

typedef struct	s_env
{
  char		**path;
  char		*home;
  char		*pwd;
  char		*oldpwd;
  char		**env;
}		t_env;

void	t_env_constructor(t_env *my_env);
int	recup_env(char **env, t_env *my_env);
int	cp_path(t_env *my_env, int i);
int	set_path(t_env *my_env);
int	to_env(char **env, t_env *my_env);

#endif /* !ENV_H_ */
