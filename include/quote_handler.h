/*
** quote_handler.h for 42sh in /home/le_l/workspace/42sh/workspace/parser
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May  8 21:10:27 2015 VIctor Le
** Last update Sat May 16 22:42:51 2015 VIctor Le
*/

#ifndef QUOTE_HANDLER_H_
# define QUOTE_HANDLER_H_

# include "t_str.h"

# define QUOTE_OK	(0)
# define QUOTE_FAIL	(1)
# define NOT_QUOTE	(2)

/*
** Gestion des quotes ' et ". Comportement identique actuellement.
** Renvoie QUOTE_OK, QUOTE_FAIL
*/
int	quote_handler(char **s, int *i, t_str *str);

#endif /* !QUOTE_HANDLER_H_ */
