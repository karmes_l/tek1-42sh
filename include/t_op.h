/*
** t_op.h for 42sh in /home/metz_a
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sat May  9 12:21:26 2015 Aurélien Metz
** Last update Sun May 24 16:23:23 2015 lionel karmes
*/

#ifndef T_OP_H_
# define T_OP_H_

# include "t_list.h"

# define NBR_OP		(6)
# define PIPE		("|")
# define OR		("||")
# define BG		("&")
# define AND		("&&")
# define NEWLINE	("\n")
# define SEMICOLON	(";")

/*
** structure contenan un op�rateurs et sa repr�sentation sous
** forme de chaine.
** @op_value:	valeurs de diff�renciation de l'op�rateur.
** @op_str:	repr�sentation de l'opr�rateur sous forme de chaine ** de
**		caract�re dans le but d'une comparaison.
*/
typedef struct	s_op
{
  char		op_value;
  char		op_str[8];
}		t_op;

/*
** initialise un tableau de structure qui va permetre d'effectuer
** des comparaison et affectations en ce qui conc�rne les
** op�rateurs.
*/
void	t_op_constructor(t_op op_handler[NBR_OP]);

/*
** transforme le dernier token de la commande en cours en
** op�rateurs de controle.
** Retour (1) si tout c'est bien pass� et (0) si ca fail.
*/
int	operator_handler(t_list *list);

#endif /* !T_OP_H_ */
