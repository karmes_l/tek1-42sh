/*
** history.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May 13 11:17:49 2015 lionel karmes
** Last update Sun May 24 15:34:07 2015 VIctor Le
*/

#ifndef HISTORY_H_
# define HISTORY_H_

# include <stdlib.h>
# include "t_list_hist.h"

/*
** 1er param : list chainé de l'historique
** 2ème param : la cmd rentré
*/
int		history(t_list_hist *, char *);

/*
** Enlève les espaces/tabulations côte à côte ou au début et fin du char *
*/
void		epur_str(char *);

/*
** Enlève tous les '\n' du char *
*/
void		delete_backspace(char *);

/*
** Retourne un pointeur sur le char * qui constitue la cmd
** dont l'index est passé en paramètre
*/
char		*find_hist(const t_list_hist *, int);

#endif /* !HISTORY_H_ */
