/*
** exec_simple_cmd.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:40:37 2015 VIctor Le
** Last update Mon May 18 09:44:30 2015 VIctor Le
*/

#ifndef EXEC_SIMPLE_CMD_H_
# define EXEC_SIMPLE_CMD_H_

# include "t_cmd.h"
# include "t_sh.h"

/*
** Execute une cmd simple, builtin ou cmd shell.
** Retourne EXEC_OK, EXEC_ERROR ou EXEC_PIPE (process fils seulement).
** @cmd:	La cmd à executer.
** @sh:		Informations nécessaires sur le shell et pour le shell.
** @forked:	Savoir si la cmd est dans un fork. Evite de refork.
*/
int	exec_simple_cmd(t_cmd *cmd, t_sh *sh, int forked);

#endif /* !EXEC_SIMPLE_CMD_H_ */
