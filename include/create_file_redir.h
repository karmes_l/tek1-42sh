/*
** create_file_redir.h for 42sh in /home/le_l/workspace/42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 22 12:32:07 2015 VIctor Le
** Last update Fri May 22 12:32:07 2015 VIctor Le
*/

#ifndef CREATE_FILE_REDIR_H_
# define CREATE_FILE_REDIR_H_

#include "t_cmd.h"

int	create_file_redir(t_cmd *cmd);

#endif /* !CREATE_FILE_REDIR_H_ */
