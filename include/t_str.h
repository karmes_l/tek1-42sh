/*
** t_str.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 17:49:23 2015 VIctor Le
** Last update Sun May 24 15:29:44 2015 VIctor Le
*/

#ifndef T_STR_H_
# define T_STR_H_

# include "t_char.h"

/*
** @head:	Début de liste d'une chaîne de caractère.
** @tail:	Fin de liste d'une chaine de caractère.
** @current:	Termcaps. Indique le caractère sous le curseur.
** @size:	Taille de la chaine.
*/
typedef struct	s_str
{
  t_char	*head;
  t_char	*tail;
  t_char	*current;
  int		size;
}		t_str;

void	t_str_constructor(t_str *str);
void	t_str_destructor(t_str *str);

/*
** Ajoute en fin de liste un caractère.
** Retourne 1, sinon 0 sur erreur (malloc).
*/
int	t_str_append_char(t_str *str, char c);

/*
** Copie la chaine de caractère passé en paramètre sous forme de char *
** dans la structure.
** Retourne 1, sinnon 0 sur erreur (t_str_append_char: malloc).
*/
int	t_str_cpy(t_str *str, const char *s);

/*
** Renvoie une chaine MALLOC contenue dans la liste, sous forme de char *.
** NULL sur erreur.
*/
char	*t_str_get_str(t_str *str);

/*
** Idem que t_str_append_char()
** Mais rajoute avant le maillon current
*/
int	t_str_append_char2(t_str *, t_char *current, char);

/*
** Crée un nouvel objet t_str *dest copié de t_str *src.
*/
int	t_str_dup(t_str *dest, t_str *src);

/*
** Crée un nouvel objet t_str *dest copié de char *cmd.
*/
int	char_to_str(t_str *dest, char *cmd);

#endif /* !T_STR_H_ */
