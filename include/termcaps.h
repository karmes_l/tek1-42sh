/*
** termcaps.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Thu May 14 14:38:33 2015 lionel karmes
** Last update Thu May 21 14:43:21 2015 VIctor Le
*/

#ifndef TERMCAPS_H_
# define TERMCAPS_H_

# include <termios.h>
# include <unistd.h>
# include <ncurses/curses.h>
# include <term.h>
# include <stdio.h>
# include "t_list_hist.h"
# include "key_events.h"
# include "t_str.h"
# include "t_sh.h"

# define FAIL (0)
# define TERM_OK (1)

# define FREE_SAVE_STR (-2)

/*
** Implicit declaration
*/
int		dprintf(int, const char *, ...);

/*
** Initialise le terminal (tty) en raw_mode, ~ECHO, ...
*/
int		init_termcaps(struct termios *, char **);

/*
** Remet le terminal en ECHO
*/
int		cano_mod(struct termios *);

/*
** Fonction principal à appeler
** Met aussi à jour list_hist si aucun problème rencontré
*/
char		*read_cmd(t_sh *);

/*
** Correspond  l'entré d'un charactère "printable"
** Ajout d'un char c à t_str *str : Si '\n' à la fin
**				    Sinon avant le curseur
*/
int		add_char_to_str(t_str *, char, int);

/*
** Correspond à la touche DEL
*/
void		char_destroy_prev(t_str *, int);

/*
** Correspond à la touche SUPPR
*/
void		char_destroy_current(t_str *, int);

/*
** Affichage de t_str *str à la position du curseur
*/
void		aff_str(int, t_str *);

/*
** Deplace le curseur à la position de la lettre t_char *current
*/
void		pos_cursor_current(int, t_str *, t_char *current);

/*
** N'est utile que pour SUPPR et DEL
*/
void		move_current_char(t_str *, t_char *, int);

/*
** Efface la fin de la ligne
** Replace le curseur à la position t_str *str->current
*/
void		update_line(int, t_str *);

/*
** Mémorise un t_str *save_str si l'on se déplace dans l'historique
** Si t_hist *hist existe Cf. commentaires sur arrow_up() et arrow_down()
*/
int		arrow_up_down(t_str *, t_hist **, int, int);

/*
** Modifie t_str *str ainsi que le curseur en fonction des flèches frappées
*/
int		arrow_handler(t_str *, t_hist **, int, int);

/*
** Efface ce qu'il y a sur le terminal puis réaffiche la ligne de cmd.
*/
void		clear_tty(int, t_str *, char *);

/*
** Détruit les t_char * de t_str *str entre t_char *begin (inclu)
** et t_char *end (exclu)
*/
void		chars_destroy(t_str *str, t_char *begin, t_char *end, int);

int		char_printable(char);
int		is_suppr(char [4]);
int		is_arrow(char [4]);
int		is_begin(char [4]);
int		is_end(char [4]);

#endif /* !TERMCAPS_H_ */
