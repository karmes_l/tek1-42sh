/*
** t_sh.h for 42sh in /home/le_l/workspace/42sh
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Mon May  4 20:40:40 2015 VIctor Le
** Last update Thu May 21 13:50:06 2015 VIctor Le
*/

#ifndef T_SH_H_
# define T_SH_H_

# include "env.h"
# include "t_list_hist.h"

/*
** @env:	Une copie de l'environnement du main et autres données.
** @hist:	Historique des cmd.
** @cmd_return:	Valeur de retour d'une commande, builtin ou pas.
**		C'est aussi la valeur de retour du main.
** @ask_exit:	Valeur pour le builtin exit. Booléen.
*/
typedef struct	s_sh
{
  t_env		env;
  t_list_hist	hist;
  char		*prompt;
  int		cmd_return;
  char		ask_exit;
}		t_sh;

void	t_sh_constructor(t_sh *sh);
void	t_sh_destructor(t_sh *sh);

#endif /* !T_SH_H_ */
