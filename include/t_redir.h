/*
** t_redir.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May  6 15:58:23 2015 lionel karmes
** Last update Sun May 24 15:31:02 2015 VIctor Le
*/

#ifndef T_REDIR_H_
# define T_REDIR_H_

# include "t_file.h"

/*
** @entry: t_file où la sortie standart sera dirigé ('>', '>>')
** @sorty: t_file où l'entrée standart sera dirigé ('<')
*/
typedef struct	s_redir
{
  t_file	*entry;
  t_file	*sorty;
}		t_redir;

t_redir		*t_redir_constructor(void);
void		t_redir_destructor(t_redir *);

#endif /* T_REDIR_H_ */
