/*
** exec_cmd.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:33:43 2015 VIctor Le
** Last update Wed May 20 07:07:32 2015 VIctor Le
*/

#ifndef EXEC_CMD_H_
# define EXEC_CMD_H_

# include "t_cmd.h"
# include "t_sh.h"

enum
  {
    EXEC_OK,
    EXEC_ERROR,
    EXEC_PIPE,
    EXEC_END
  };

/*
** Exécute une cmd simple ou listée
** Retourne EXEC_OK, EXEC_ERROR ou EXEC_PIPE (process fils seulement).
*/
int	exec_cmd(t_cmd *cur_cmd, t_sh *sh);

#endif /* !EXEC_CMD_H_ */
