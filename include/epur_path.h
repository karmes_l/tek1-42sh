/*
** epur_path.h for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 14 21:35:43 2015 VIctor Le
** Last update Sat May 16 08:32:39 2015 VIctor Le
*/

#ifndef EPUR_PATH_H_
# define EPUR_PATH_H_

/*
** Supprime des éléments superflus du path donné.
*/
void	epur_path(char *path);

#endif /* !EPUR_PATH_H_ */
