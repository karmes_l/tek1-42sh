/*
** is_correct_quoted.h for 42sh in /home/le_l/workspace/42sh
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 22 14:14:10 2015 VIctor Le
** Last update Fri May 22 14:14:28 2015 VIctor Le
*/

#ifndef IS_CORRECT_QUOTED_H_
# define IS_CORRECT_QUOTED_H_

/*
** Vérifie que les quotes sont bien fermés.
** Retourne 1 (OK), 0 (FAIL)
*/
int	is_correct_quoted(char *s);

#endif /* !IS_CORRECT_QUOTED_H_ */
