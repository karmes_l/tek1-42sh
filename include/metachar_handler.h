/*
** metachar_handler.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Wed May  6 15:07:51 2015 VIctor Le
** Last update Mon May 18 09:40:17 2015 VIctor Le
*/

#ifndef METACHAR_HANDLER_H_
# define METACHAR_HANDLER_H_

# include "t_str.h"
# include "t_list.h"

# define NB_META	(8)
# define METACHARS	"|&;<> \t\n"

# define META_OK	(0)
# define META_FAIL	(1)
# define NOT_META	(2)

typedef int	(*t_meta)(char *, int *, t_str *, t_list *);

/*
** Fonction principale de gestion des metacaractères.
** Renvoie META_OK, si pas d'erreur pdt récupération du metacaractère.
**         META_FAIL, si erreur de malloc pdt récupération du metacaractère.
**         NOT_META, si s[*i] n'est pas un metacaractère.
*/
int	metachar_handler(char *s, int *i, t_str *str, t_list *list);

/*
** Fonctions de gestion d'un metacaractère en particulier.
** Retour IDENTIQUE a metachar_handler.
*/
int	pipe_handler(char *s, int *i, t_str *str, t_list *list);
int	ampersand_handler(char *s, int *i, t_str *str, t_list *list);
int	semicolon_handler(char *s, int *i, t_str *str, t_list *list);
int	open_chevron_handler(char *s, int *i, t_str *str, t_list *list);
int	close_chevron_handler(char *s, int *i, t_str *str, t_list *list);
int	blank_handler(char *s, int *i, t_str *str, t_list *list);
int	newline_handler(char *s, int *i, t_str *str, t_list *list);
/*
** Recup un token d'un t_str et la place à la fin des tokens d'un t_cmd.
** Renvoie 1 si la chaine est vide ou sur succes, 0 sur erreur.
*/
int	push_back_token(t_str *str, t_cmd *last_cmd, int isword);

#endif /* !METACHAR_HANDLER_H_ */
