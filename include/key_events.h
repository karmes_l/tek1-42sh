/*
** key_events.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/termcaps/include
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May 15 00:14:38 2015 lionel karmes
** Last update Sun May 24 15:33:13 2015 VIctor Le
*/

#ifndef KEY_EVENTS_H_
# define KEY_EVENTS_H_

# define CTRL_D (2)
# define CTRL_K (11)
# define CTRL_L (12)
# define CTRL_U (21)
# define CTRL_W (23)
# define DEL (127)
# define K_UP 65
# define K_DOWN 66
# define K_RIGHT 67
# define K_LEFT 68

#endif /* !KEY_EVENTS_H_ */
