/*
** redirection.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May  8 17:10:01 2015 lionel karmes
** Last update Sat May 23 12:12:26 2015 VIctor Le
*/

#ifndef REDIRECTION_H_
# define REDIRECTION_H_

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <errno.h>
# include <string.h>
# include "t_list.h"
# include "t_redir.h"

# define CHEV_R ">"
# define CHEV_DR ">>"
# define CHEV_L "<"
# define CHEV_DL "<<"
# define CHEV_TL "<<<"

# define FAIL 0
# define OK 1
# define END_HERE_DOC 2

/*
** Gestion de la redirection '<<'
** Remple le char **buffer de t_cmd
** Fonction a appellé dans un fork si on a besoin de fork
** A appellé avant handle_redirection()
*/
int	here_doc(t_cmd *);

/*
** Gestion principal de toutes les redirections
** Fonction a appellé dans un fork si on a besoin de fork
** A appeller après here_doc()
*/
int	handle_redirections(t_cmd *);

void	close_fd(int);

/*
** Renvoie le fd d'un fichier open
*/
int	open_file(t_file *);

/*
** Fonctions contenue dans un tableau de pointeur sur fonctions
*/
int	chevron_right(t_redir *, char *, char *);
int	chevron_double_right(t_redir *, char *, char *);
int	chevron_left(t_redir *, char *, char *);
int	chevron_double_left(t_redir *, char *, char *);

#endif /* REDIRECTION_H_ */
