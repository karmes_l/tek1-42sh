/*
** gnl.h for 42sh in /home/le_l/workspace/42sh/workspace/parser/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May  9 15:38:55 2015 VIctor Le
** Last update Mon May 18 09:40:45 2015 VIctor Le
*/

#ifndef GNL_H_
# define GNL_H_

# define GNL_READSIZE	(100)

/*
** Renvoie NULL sur erreur de read(),  "" sur lecture de chaine vide
** Sinon, renvoie la ligne lue (malloc).
*/
char	*gnl(const int fd);

#endif /* !GNL_H_ */
