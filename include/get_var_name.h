/*
** get_var_name.h for 42sh in /home/le_l/workspace/42sh/workspace/builtin/unsetenv
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 13:32:06 2015 VIctor Le
** Last update Mon May 18 09:41:51 2015 VIctor Le
*/

#ifndef GET_VAR_NAME_H_
# define GET_VAR_NAME_H_

/*
** Permet d'obtenir une chaine avec uniquement
** le nom de la variable d'environnement.
** Retourne une chaine (malloc).
*/
char	*get_var_name(char *env_line);

#endif /* !GET_VAR_NAME_H_ */
