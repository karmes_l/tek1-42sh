/*
** exec_pipe.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:36:12 2015 VIctor Le
** Last update Sat May 16 07:36:13 2015 VIctor Le
*/

#ifndef EXEC_PIPE_H_
# define EXEC_PIPE_H_

# include "t_cmd.h"
# include "t_sh.h"

/*
** Execute une série de cmd pipée.
** Retourne EXEC_OK, EXEC_ERROR ou EXEC_PIPE (process fils seulement).
*/
int	exec_pipe(t_cmd *cmd, t_sh *sh);

#endif /* !EXEC_PIPE_H_ */
