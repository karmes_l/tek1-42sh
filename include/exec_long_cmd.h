/*
** exec_long_cmd.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 07:37:21 2015 VIctor Le
** Last update Sat May 16 07:37:22 2015 VIctor Le
*/

#ifndef EXEC_LONG_CMD_H_
# define EXEC_LONG_CMD_H_

# include "t_cmd.h"
# include "t_sh.h"

/*
** Exécute une cmd listée, càd composée de |, &&  ou ||
** Retourne EXEC_OK, EXEC_ERROR, EXEC_PIPE (process fils seulement).
*/
int	exec_long_cmd(t_cmd *cmd, t_sh *sh);

#endif /* !EXEC_LONG_CMD_H_ */
