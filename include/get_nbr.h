/*
** get_nbr.h for 42sh in /home/le_l/rendu/Unix/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 16:00:43 2015 VIctor Le
** Last update Sun May 24 15:34:33 2015 VIctor Le
*/

#ifndef GET_NBR_H_
# define GET_NBR_H_

int	is_nbr(char *nb);

/*
** Get_nbr avec gestion de l'overflow.
** 0 sur erreur.
*/
int	get_nbr(const char *nb);

#endif /* !GET_NBR_H_ */
