/*
** t_char.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 17:52:42 2015 VIctor Le
** Last update Mon May 18 13:07:04 2015 VIctor Le
*/

#ifndef T_CHAR_H_
# define T_CHAR_H_

/*
** @next:	Caractère suivant.
** @prev:	Caractère précédent.
** @c:		Caractère d'une chaine.
*/
typedef struct	s_char
{
  struct s_char	*next;
  struct s_char	*prev;
  char		c;
}		t_char;

void	t_char_constructor(t_char *c);
void	t_char_destructor(t_char *c);

#endif /* !T_CHAR_H_ */
