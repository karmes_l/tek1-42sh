/*
** parser.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 17:46:59 2015 VIctor Le
** Last update Fri May  8 21:36:17 2015 VIctor Le
*/

#ifndef PARSER_H_
# define PARSER_H_

# include "t_list.h"

/*
** Decoupe la ligne de cmd oen tokens répartis dans des cmd.
** Les opérateurs de contrôle sont convertis en valeur.
** Renvoie 1 si pas d'erreur, 0 sur une erreur.
*/
int	parser(char *s, t_list *list);

#endif /* !PARSER_H_ */
