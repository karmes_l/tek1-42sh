/*
** t_hist.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed May 13 10:34:18 2015 lionel karmes
** Last update Wed May 13 11:26:14 2015 lionel karmes
*/

#ifndef T_HIST_H_
# define T_HIST_H_

/*
** @next : maillon suivant
** @prev : maillon précédent
** @cmd : cmd ayant subit un epur_str (pas d'espace/tab à côté)
*/
typedef struct	s_hist
{
  struct s_hist	*next;
  struct s_hist	*prev;
  char		*cmd;
}		t_hist;

void		t_hist_constructor(t_hist *hist);
t_hist		*t_hist_destructor(t_hist *hist);

#endif /* !T_HIST_H_ */
