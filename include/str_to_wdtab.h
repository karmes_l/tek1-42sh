/*
** str_to_wdtab.h for 42sh in /home/le_l/workspace/42sh/workspace/parser/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 09:07:27 2015 VIctor Le
** Last update Fri May 15 12:02:37 2015 VIctor Le
*/

#ifndef STR_TO_WDTAB_H_
# define STR_TO_WDTAB_H_

/*
** str_to_wdtab générique.
** Delim a rentrer dans une chaine avec un \0.
** Retourne le wdtab, NULL sur erreur.
*/
char	**str_to_wdtab(char *s, char *delim);

/*
** Fonction pour free le wdtab.
*/
void	free_wdtab(char **wdtab);

#endif /* STR_TO_WDTAB_H_ */
