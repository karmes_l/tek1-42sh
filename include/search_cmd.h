/*
** search_cmd.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 10:30:45 2015 VIctor Le
** Last update Sat May 16 10:31:08 2015 VIctor Le
*/

#ifndef SEARCH_CMD_H_
# define SEARCH_CMD_H_

char	*search_cmd(char **path, char *bin);

#endif /* !SEARCH_CMD_H_ */
