/*
** create_var_env.h for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Fri May 15 00:07:08 2015 VIctor Le
** Last update Mon May 18 09:45:59 2015 VIctor Le
*/

#ifndef CREATE_VAR_ENV_H_
# define CREATE_VAR_ENV_H_

/*
** Ajoute une nouvelle variable d'env.
** Retourne l'env, NULL sur erreur.
** Danger l'env peut avoir été free après l'opération.
*/
char	**create_var_env(char **env, char *new_var, char *value);

#endif /* !CREATE_VAR_ENV_H_ */
