/*
** read_signal.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/signal
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Tue May 12 17:35:41 2015 lionel karmes
** Last update Fri May 29 16:35:51 2015 VIctor Le
*/

#ifndef READ_SIGNAL_H_
# define READ_SIGNAL_H_

# include <signal.h>

# define PROC_DAD	1
# define PROC_SON	0

/*
** Fonction à appeller avant un get_next_line
** Son paramètre et soit PROC_DAD ou PROC_SON
** suivant le processus où l'on se situe
*/
void		read_signal(int);

void		get_sigchld(int sign);
void		get_sigquit_son(int sign);
void		get_sigquit_dad(int sign);
void		get_sigint_son(int sign);
void		get_sigint_dad(int sign);

#endif /* !READ_SIGNAL_H_ */
