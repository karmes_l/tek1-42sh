/*
** exec_shell_cmd.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 08:21:42 2015 VIctor Le
** Last update Sat May 16 08:21:44 2015 VIctor Le
*/

#ifndef EXEC_SHELL_CMD_H_
# define EXEC_SHELL_CMD_H_

# include "t_cmd.h"
# include "t_sh.h"

/*
** Exécute une cmd shell.
** Retourne EXEC_OK, EXEC_ERROR ou EXEC_PIPE (process fils seulement).
*/
int	exec_shell_cmd(t_cmd *cmd, t_sh *sh, char *bin_path, int forked);

#endif /* !EXEC_SHELL_CMD_H_ */
