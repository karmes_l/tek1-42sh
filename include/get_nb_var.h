/*
** get_nb_var.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/srcs
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 10:36:57 2015 VIctor Le
** Last update Mon May 18 09:42:18 2015 VIctor Le
*/

#ifndef GET_NB_VAR_H_
# define GET_NB_VAR_H_

/*
** Retourne le nombre de variable d'env dans env.
** 0 si env == NULL.
*/
int	get_nb_var(char **env);

#endif /* !GET_NB_VAR_H_ */
