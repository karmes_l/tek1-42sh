/*
** echo.h for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh/workspace/parser
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Sat May 16 17:37:28 2015 Aurélien Metz
** Last update Wed May 20 07:06:58 2015 VIctor Le
*/

#ifndef ECHO_H_
# define ECHO_H_

# include "t_token.h"

void	echo(t_token *token);

#endif /* !ECHO_H_ */
