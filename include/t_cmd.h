/*
** t_cmd.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 08:45:19 2015 VIctor Le
** Last update Fri May 22 16:19:48 2015 VIctor Le
*/

#ifndef T_CMD_H_
# define T_CMD_H_

# include "t_token.h"

/*
** OP_VOID, valeur initiale de la variable 'operator' dans 't_cmd'.
*/
enum	e_op
  {
    OP_VOID,
    OP_PIPE,
    OP_OR,
    OP_AND,
    OP_NEWLINE,
    OP_SEMICOLON,
    OP_BG
  };

/*
** Structure contenant une cmd représentée par une liste de tokens.
** @next:	Cmd suivante.
** @prev	Cmd précédente.
** @head:	Début de liste de la liste des tokens de la cmd.
** @tail:	Fin de liste de la liste des tokens de la cmd.
** @list_size:	Taille de la liste.
** @ret:	Retour de la cmd.
** @operator:	Indique quel operateur se trouve après la cmd.
**		Liste des opérateurs --> voir e_op
*/
typedef struct	s_cmd
{
  struct s_cmd	*next;
  struct s_cmd	*prev;
  t_token	*head;
  t_token	*tail;
  char		**buffer;
  int		list_size;
  int		ret;
  char		operator[2];
}		t_cmd;

void	t_cmd_constructor(t_cmd *cmd);

/*
** Renvoie la cmd suivante.
*/
t_cmd	*t_cmd_destructor(t_cmd *cmd);

/*
** Ajoute un token initialisé en fin de liste des tokens d'une cmd.
** Return: 0 (FAIL), 1 (OK).
*/
int	t_cmd_append_token(t_cmd *cmd, char *token, char isword);

/*
** Retourne sous forme de tableau tous les tokens de la cmd.
** Retourne NULL sur erreur.
*/
char	**t_cmd_get_argv(t_cmd *cmd);

/*
** Permet de free le tableau alloué avec la fonction t_cmd_get_argv().
*/
void	t_cmd_free_argv(char **argv);

/*
** Retire un token d'une cmd.
** Retourne le token suivant au token retiré.
*/
t_token *t_cmd_remove_token(t_cmd *cmd, t_token *token);

#endif /* !T_CMD_H_ */
