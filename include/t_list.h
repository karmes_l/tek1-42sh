/*
** t_list.h for 42sh in /home/le_l/workspace/42sh/tmp_Victor/pour_Aurelien
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Tue May  5 07:41:03 2015 VIctor Le
** Last update Fri May  8 00:00:21 2015 VIctor Le
*/

#ifndef T_LIST_H_
# define T_LIST_H_

# include "t_cmd.h"

/*
** Structure de contrôle contenant toutes les cmds entrées.
** @head:	Début de liste.
** @tail:	Fin de liste.
** @list_size:	Taille de la liste.
*/
typedef struct	s_list
{
  t_cmd		*head;
  t_cmd		*tail;
  int		list_size;
}		t_list;

void	t_list_constructor(t_list *list);
void	t_list_destructor(t_list *list);

/*
** Ajoute une cmd vide en fin de liste des cmd.
** Renvoie 1, 0 si erreur.
*/
int	t_list_append_empty_cmd(t_list *list);

/*
** Retire une cmd de la liste.
*/
void	t_list_remove_cmd(t_list *list, t_cmd *cmd);

#endif /* !T_LIST_H_ */
