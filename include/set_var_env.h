/*
** set_var_env.h for 42sh in /home/le_l/workspace/42sh/workspace/builtin/cd
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Thu May 14 18:43:35 2015 VIctor Le
** Last update Sat May 16 10:05:00 2015 VIctor Le
*/

#ifndef SET_VAR_ENV_H_
# define SET_VAR_ENV_H_

/*
** Retourne l'env.
** Si la variable existe, la fonction remalloc une nouvelle chaine
** Si la variable n'existe pas, elle remalloc un nouveau tableau
** avec la nouvelle chaine.
** Danger: l'env ou la variable a modifié peuvent
** avoir étés free après l'opération.
*/
char	**set_var_env(char **env, char *var_name, char *value);

#endif /* !SET_VAR_ENV_H_ */
