/*
** builtins.h for 42sh in /home/le_l/workspace/tmp/PSU_2014_42sh/include
**
** Made by VIctor Le
** Login   <le_l@epitech.net>
**
** Started on  Sat May 16 08:26:15 2015 VIctor Le
** Last update Sat May 23 13:09:28 2015 VIctor Le
*/

#ifndef BUILTINS_H_
# define BUILTINS_H_

# include "t_cmd.h"
# include "t_sh.h"

# define NB_BUILTIN		(7)
# define BUILTIN_HISTORY	"history"
# define BUILTIN_CD		"cd"
# define BUILTIN_ECHO		"echo"
# define BUILTIN_ENV		"env"
# define BUILTIN_SETENV		"setenv"
# define BUILTIN_UNSETENV	"unsetenv"
# define BUILTIN_EXIT		"exit"

# define HIST_OPTIONS		"chr"

typedef int	(*t_builtin)(t_cmd *, t_sh *);

enum	e_hist_flag
  {
    HIST_F_C = 1,
    HIST_F_H = 2,
    HIST_F_R = 4
  };

int	env_err(char **env, const char *err);

int	builtin_history(t_cmd *cmd, t_sh *sh);
int	builtin_cd(t_cmd *cmd, t_sh *sh);
int	builtin_echo(t_cmd *cmd, t_sh *sh);
int	builtin_setenv(t_cmd *cmd, t_sh *sh);
int	builtin_unsetenv(t_cmd *cmd, t_sh *sh);
int	builtin_env(t_cmd *cmd, t_sh *sh);
int	builtin_exit(t_cmd *cmd, t_sh *sh);

char	**env_option(t_cmd *cmd, char **env);
char	**cpy_env(char **env, t_sh *sh);
char	**env_unset(char **env, char *name);
char	**env_ignore(char **env, char *name);
char	**env_zero(char **env, char *name);
char	**version(char **env, char *name);
char	**help(char **env, char *name);
char	**print_env(char **env);

void	init_tab(char **tab, char **(*fptr[9])(char**, char *));
void	my_putstr(char *str);
void	my_putchar(char c);

#endif /* !BUILTINS_H_ */
