/*
** lexer.h for 42sh in /home/metz_a/rendu/System unix/PSU_2014_42sh/workspace/parser
**
** Made by Aurélien Metz
** Login   <metz_a@epitech.net>
**
** Started on  Mon May 11 13:35:04 2015 Aurélien Metz
** Last update Sun May 24 15:33:01 2015 VIctor Le
*/

#ifndef LEXER_H_
# define LEXER_H_

# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <stdio.h>
# include "t_list.h"

# define MISSING_FILE	("\033[31mMissing name for redirect.\n\033[0m")
# define DOESNT_EXIST	("\033[31m%s: No such file or directory.\n\033[0m")
# define RIGHT_RED	('>')
# define LEFT_RED	('<')
# define CMD_NULL	("\033[31mInvalid null command.\n\033[0m")
# define CMD_AMB	("\033[31mAmbiguous %s redirect.\n\033[0m")
# define OUTPUT		("output")
# define INPUT		("input")

/*
** Vérifie que la cmd à éxécutée est correcte.
** Renvoie 1, 0 sur erreur.
*/
int	lexer(const t_list *list);

int	check_token(const t_token *token, char *output, char *input);

int	find_word(const t_token *token);

#endif /* !LEXER_H_ */
