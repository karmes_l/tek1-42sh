/*
** t_list_hist.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel/history
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Wed May 13 10:35:55 2015 lionel karmes
** Last update Thu May 21 14:35:13 2015 VIctor Le
*/

#ifndef T_LIST_HIST_H_
# define T_LIST_HIST_H_

# include "t_hist.h"

typedef struct	s_list_hist
{
  t_hist	*head;
  t_hist	*tail;
  t_hist	*it;
  int		id_current;
  int		list_size;
}		t_list_hist;

void		t_list_hist_constructor(t_list_hist *list_hist);
void		t_list_hist_destructor(t_list_hist *list_hist);

int		t_list_hist_append_hist(t_list_hist *list_hist, char *cmd);

void		t_list_hist_remove_hist(t_list_hist *list_hist, t_hist *hist);

#endif /* !T_LIST_HIST */
