/*
** t_file.h for  in /home/karmes_l/Projets/Systeme_Unix/42sh/PSU_2014_42sh/tmp_Lionel
**
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
**
** Started on  Fri May  8 12:16:57 2015 lionel karmes
** Last update Sun May 24 16:32:15 2015 lionel karmes
*/

#ifndef T_FILE_H_
# define T_FILE_H_

/*
** @path: nom du fichier
** @flag: flag d'ouverture du fichier (O_TRUNC, O_CREAT ...)
*/
typedef struct	s_file
{
  char		*path;
  int		flags;
}		t_file;

t_file		*t_file_constructor(char *, int);
void		t_file_destructor(t_file *);

#endif /* T_FILE_H_ */
